#ifndef MOVECOMMAND_HPP_INCLUDED
#define MOVECOMMAND_HPP_INCLUDED
#include "Command.hpp"
#include <memory>
#include <iostream>
namespace snake {
    class MoveUpCommand : public Command {
    public:
        MoveUpCommand(Player* player)
        : Command(player) {
        }
        void execute() override {
            mReceiver->moveUp();
        }
    };
    class MoveDownCommand : public Command {
    public:
        MoveDownCommand(Player* player)
        : Command(player) {
        }
        void execute() override {
            mReceiver->moveDown();
        }
    };
    class MoveLeftCommand : public Command {
    public:
        MoveLeftCommand(Player* player)
        : Command(player) {
        }
        void execute() override {
            mReceiver->moveLeft();
        }
    };
    class MoveRightCommand : public Command {
    public:
        MoveRightCommand(Player* player)
        : Command(player) {
        }
        void execute() override {
            mReceiver->moveRight();
        }
    };
}
#endif // MOVECOMMAND_HPP_INCLUDED
