#ifndef INCREMENTTAILCOMMAND_HPP_INCLUDED
#define INCREMENTTAILCOMMAND_HPP_INCLUDED
#include "Command.hpp"
#include <memory>
#include <iostream>
namespace snake {
    class IncrementTailCommand : public Command {
    public:
        IncrementTailCommand(Player* player)
        : Command(player) {
        }
        void execute() override {
            mReceiver->increaseTail();
        }
    };
}
#endif // INCREMENTTAILCOMMAND_HPP_INCLUDED
