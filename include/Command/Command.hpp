#ifndef COMMAND_HPP_INCLUDED
#define COMMAND_HPP_INCLUDED
#include "../Player/Player.hpp"
#include <memory>
namespace snake {
    class Command {
    public:
        Command(Player* entity) : mReceiver(entity) { }
        virtual ~Command() { }
        virtual void execute() { }
    protected:
        Player* mReceiver;
    };
    using CommandPtr = std::unique_ptr<Command>;
}
#endif // COMMAND_HPP_INCLUDED
