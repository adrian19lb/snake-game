#ifndef MAP_HPP_INCLUDED
#define MAP_HPP_INCLUDED
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include <sstream>
#include <exception>
#include "../ResourceHolder/ResourceHolder.hpp"
namespace snake {
    class Map : public sf::Drawable, public sf::Transformable {
    public:
        Map(TextureHolder& holder) : mTileset(holder.get(Entity::Map)) { }
        void create(sf::Vector2u tileSize);
    private:
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    private:
        sf::VertexArray mVertices;
        sf::Texture& mTileset;
    };
}
#endif // MAP_HPP_INCLUDED
