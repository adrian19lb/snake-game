#ifndef GAMEOVERSTATE_HPP_INCLUDED
#define GAMEOVERSTATE_HPP_INCLUDED
#include "../IState/IState.hpp"
#include "../StateManager/StateManager.hpp"
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <vector>
#include <memory>
#include "../Button/Button.hpp"
namespace snake {
    class GameOverState: public IState {
    public:
        GameOverState(StateManager& stateManager, GameContext context);
        void handleInput(sf::Event& event) override;
        void update(sf::Time dt) override;
        void draw() override;
    private:
        std::vector<std::unique_ptr<Button>> mButtons;
    };
}
#endif // GAMEOVERSTATE_HPP_INCLUDED
