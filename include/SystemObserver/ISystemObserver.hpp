#ifndef ICOMMANDEXECUTOR_HPP_INCLUDED
#define ICOMMANDEXECUTOR_HPP_INCLUDED
#include "../Command/Command.hpp"
namespace snake {
    class ISystemObserver {
    public:
        virtual void receive(CommandPtr& command) = 0;
    };
}
#endif // ICOMMANDEXECUTOR_HPP_INCLUDED
