#ifndef GAMESTATE_HPP_INCLUDED
#define GAMESTATE_HPP_INCLUDED
#include "../IState/IState.hpp"
#include "../World/World.hpp"
namespace snake {

    class GameState : public IState {
    public:
        GameState(StateManager& stateManager, GameContext context);
        void handleInput(sf::Event& event) override;
        void update(sf::Time dt) override;
        void draw() override;
    private:
        void checkRequest();
    private:
        World mWorld;
    };
}
#endif // GAMESTATE_HPP_INCLUDED
