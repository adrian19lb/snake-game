#ifndef RESOURCEIDENTIFIER_HPP_INCLUDED
#define RESOURCEIDENTIFIER_HPP_INCLUDED
namespace snake::Entity {
    enum ID {
        Player,
        Apple,
        Map,
        Button,
        MuteButton,
        UnmuteButton,
        GamePlay,
        Pause,
        GameOver,
        Menu,
        Win,
        Credits
    };
}
namespace snake::Audio {
    enum ID {
        Eating,
        Dying,
        Click,
        Moving,
        SpawnItem,
        Winning,
        ButtonSelect,
        ButtonClick
    };
}
namespace snake::States {
    enum ID {
        None,
        Win,
        MainMenu,
        Playing,
        Pause,
        GameOver,
        Credits
    };
}

#endif // RESOURCEIDENTIFIER_HPP_INCLUDED
