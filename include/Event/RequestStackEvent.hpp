#ifndef REQUESTSTACKEVENT_HPP_INCLUDED
#define REQUESTSTACKEVENT_HPP_INCLUDED
#include "Event.hpp"
#include "../IState/IState.hpp"
#include "../StateManager/StateManager.hpp"
namespace snake {
    class RequestStackEvent : public Event<RequestStackEvent> {
    public:
        RequestStackEvent(StateManager::Action actionID = StateManager::None, States::ID stateID = States::None)
        : mActionID(actionID)
        , mStateID(stateID) {
        }
        explicit RequestStackEvent(const RequestStackEvent& event)
        : mActionID(event.getActionID())
        , mStateID (event.getStateID()) {
        }
        States::ID getStateID() const {
            return mStateID;
        }
        StateManager::Action getActionID() const {
            return mActionID;
        }
    private:
        States::ID mStateID;
        StateManager::Action mActionID;
    };
}


#endif // REQUESTSTACKEVENT_HPP_INCLUDED
