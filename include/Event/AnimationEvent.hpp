#ifndef ANIMATIONEVENT_HPP_INCLUDED
#define ANIMATIONEVENT_HPP_INCLUDED
#include "Event.hpp"
#include <string>
namespace snake {
    class AnimationEvent : public Event<AnimationEvent> {
    public:
        AnimationEvent(const std::string& action, const std::string& prevAction)
        : mAction(action)
        , mPrevAction(prevAction) {
        }
        const std::string& getAction() { return mAction; }
        const std::string& getPrevAction() { return mPrevAction; }
    private:
        std::string mAction;
        std::string mPrevAction;
    };
}
#endif // ANIMATIONEVENT_HPP_INCLUDED
