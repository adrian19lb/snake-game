#ifndef ITEMSPAWNEREVENT_HPP_INCLUDED
#define ITEMSPAWNEREVENT_HPP_INCLUDED
#include "Event.hpp"
#include "../ResourceIdentifier/ResourceIdentifier.hpp"
namespace snake {
    class ItemSpawnerEvent : public Event<ItemSpawnEvent> {
    public:
        ItemSpawnEvent(Entity::ID entityID) : mEntityID(entityID) { }
        Entity::ID getEntityID() { return mEntityID; }
    private:
        Entity::ID mEntityID;
    };
}
#endif // ITEMSPAWNEREVENT_HPP_INCLUDED
