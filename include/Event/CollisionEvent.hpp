#ifndef COLLISIONEVENT_HPP_INCLUDED
#define COLLISIONEVENT_HPP_INCLUDED
#include "Event.hpp"
#include "../ResourceIdentifier/ResourceIdentifier.hpp"
namespace snake {
    class CollisionEvent : public Event<CollisionEvent> {
    public:
        CollisionEvent(Entity::ID e1, Entity::ID e2) {
            mEntities.first = e1;
            mEntities.second = e2;
        }
        CollisionEvent() {
        }
        const std::pair<Entity::ID, Entity::ID>& getEntities() {
            return mEntities;
        }
    private:
        std::pair<Entity::ID, Entity::ID> mEntities;
    };
}


#endif // COLLISIONEVENT_HPP_INCLUDED
