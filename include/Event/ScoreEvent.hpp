#ifndef SCOREEVENT_HPP_INCLUDED
#define SCOREEVENT_HPP_INCLUDED
#include "Event.hpp"
namespace snake {
    class ScoreEvent : public Event<ScoreEvent> {
    public:
        ScoreEvent() { }
        ~ScoreEvent() { }
    };
}


#endif // SCOREEVENT_HPP_INCLUDED
