#ifndef ITEMSPAWNEVENT_HPP_INCLUDED
#define ITEMSPAWNEVENT_HPP_INCLUDED
#include "Event.hpp"
#include "../Item/Item.hpp"
#include <memory>
namespace snake {
     class ItemSpawnEvent : public Event<ItemSpawnEvent> {
     public:
        ItemSpawnEvent(std::unique_ptr<Item> item)
        : mItem(std::move(item)) {
        }
        std::unique_ptr<Item> getItem() {
            return std::move(mItem);
        }
     private:
        std::unique_ptr<Item> mItem;
     };
}

#endif // ITEMSPAWNEVENT_HPP_INCLUDED
