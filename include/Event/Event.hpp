#ifndef EVENT_HPP_INCLUDED
#define EVENT_HPP_INCLUDED
#include "BaseEvent.hpp"
namespace snake {
    template<typename EventType>
    class Event : public BaseEvent {
    public:
        static unsigned getID() { return mID; }
    protected:
        static unsigned mID;
    };
    template<typename EventType>
    unsigned Event<EventType>::mID = BaseEvent::nextID++;
}


#endif // EVENT_HPP_INCLUDED
