#ifndef AUDIOEVENT_HPP_INCLUDED
#define AUDIOEVENT_HPP_INCLUDED
#include "Event.hpp"
#include "../ResourceIdentifier/ResourceIdentifier.hpp"
namespace snake {
    class AudioEvent : public Event<AudioEvent> {
    public:
        AudioEvent(Audio::ID audioID)
        : mAudioID(audioID) {
        }
        Audio::ID getAudio() {
            return mAudioID; }
    private:
        Audio::ID mAudioID;
    };
}


#endif // AUDIOEVENT_HPP_INCLUDED
