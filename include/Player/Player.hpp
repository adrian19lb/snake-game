#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED
#include <deque>
#include <memory>
#include <algorithm>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/System/Time.hpp>
#include "../ResourceHolder/ResourceHolder.hpp"
namespace snake {
    class Player : public sf::Drawable, public sf::Transformable {
    private:
        struct Segment {
            Segment(const sf::Vector2f& coordinates, const TextureHolder& holder);
            Segment(const Segment& segment, const TextureHolder& holder);
            Segment(const Segment& segment);
            sf::Vector2f mCoordinates;
            sf::Sprite mSprite;
        };
    public:
        using SegmentPtr = std::unique_ptr<Segment>;
    public:
        Player(const sf::Vector2f& startPosition, TextureHolder& holder) :
            mMovementStep(36.f),
            mID(Entity::Player) {
                mTail.emplace_back(std::move(std::make_unique<Segment>(startPosition, holder)));
        }
        void increaseTail();
        void decreaseTail();
        unsigned getSize() const;
        Segment& getHead();
        Segment& getTail();
        sf::Sprite& getSprite();
        const Entity::ID getID();
        Segment& operator[](unsigned segmentIndex);
        void moveRight();
        void moveUp();
        void moveDown();
        void moveLeft();
    private:
        void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
        void makeMovement(const sf::Vector2f& movement);
    private:
        std::deque<SegmentPtr> mTail;
        float mMovementStep;
        Entity::ID mID;
    };
    using PlayerPtr = std::shared_ptr<Player>;
}

#endif // PLAYER_HPP_INCLUDED
