#ifndef MOVEMENTSYSTEM_HPP_INCLUDED
#define MOVEMENTSYSTEM_HPP_INCLUDED
#include <vector>
#include "../SystemObserver/ISystemObserver.hpp"
#include "../Event/AudioEvent.hpp"
#include "../ResourceIdentifier/ResourceIdentifier.hpp"
#include "../EventManager/EventManager.hpp"
namespace snake {
    class MovementSystem : public ISystemObserver {
    public:
        MovementSystem();
        ~MovementSystem() { }
        void receive(CommandPtr& command) override;
        void process();
    private:
        std::vector<CommandPtr> mMoveCommand;
    };
}


#endif // MOVEMENTSYSTEM_HPP_INCLUDED
