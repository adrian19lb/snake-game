#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include "../ResourceHolder/ResourceHolder.hpp"
#include "../StateManager/StateManager.hpp"
#include "../WinState/WinState.hpp"
#include "../MenuState/MenuState.hpp"
#include "../GameState/GameState.hpp"
#include "../PauseState/PauseState.hpp"
#include "../CreditsState/CreditsState.hpp"
#include "../GameOverState/GameOverState.hpp"
#include "../Constants/Constants.hpp"

namespace snake {
    class Game {
    public:
        ~Game();
        Game();
    private:
        void loadTexturesFromFiles();
        void loadFontsFromFiles();
        void loadSoundsFromFiles();
        void loadMusicsFromFiles();
        void registerStates();
    public:
        void run();
    private:
        void runFixedTimeStepLoop();
        void updateLogic();
    private:
        void processInput();
        void render();
        bool checkRequestToCloseWindow();
    private:
       sf::RenderWindow gameWindow;
       sf::Time timeStep;
       TextureHolder textures;
       FontHolder fonts;
       SoundHolder sounds;
       MusicHolder musics;
       StateManager stateManager;
    };
}

#endif // GAME_HPP_INCLUDED
