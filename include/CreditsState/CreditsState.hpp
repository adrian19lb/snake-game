#ifndef CREDITSSTATE_HPP_INCLUDED
#define CREDITSSTATE_HPP_INCLUDED
#include "../IState/IState.hpp"
#include "../StateManager/StateManager.hpp"
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics/Text.hpp>
#include "../Button/Button.hpp"
#include <memory>
#include <string>
#include <fstream>
#include <exception>
#include <vector>
namespace snake {
    class CreditsState : public IState {
    public:
        CreditsState(StateManager& stateManager, GameContext context);
        void handleInput(sf::Event& event) override;
        void update(sf::Time dt) override;
        void draw() override;
    private:
        std::unique_ptr<Button> mButton;
        std::vector<sf::Text> mCredits;

    };
}
#endif // CREDITSSTATE_HPP_INCLUDED
