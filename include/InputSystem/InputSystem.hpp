#ifndef INPUTSYSTEM_HPP_INCLUDED
#define INPUTSYSTEM_HPP_INCLUDED
#include <SFML/Graphics/RenderWindow.hpp>
#include <memory>
#include <string>
#include "../Command/MoveCommand.hpp"
#include "../SystemSubject/ISystemSubject.hpp"
#include "../EventManager/EventManager.hpp"
#include "../Event/AnimationEvent.hpp"
#include "../Event/AudioEvent.hpp"
#include "../Event/RequestStackEvent.hpp"
#include <SFML/Window/Event.hpp>
namespace snake {

    enum Direction {
        Vertical,
        Horizontal
    };

    class InputSystem : public SystemSubject {
    public:
        InputSystem(sf::RenderWindow& window, EventManager& eventManager);
        void process(Player* gameActor, sf::Event& event);
    private:
        void notify(CommandPtr command) override;
        void createAction();
    private:
        sf::RenderWindow& mGameWindow;
        EventManager& mEventManager;
        std::string mCurrentAction;
        std::string mPreviousAction;
        Direction mDirection;
    };
}
#endif // INPUTSYSTEM_HPP_INCLUDED
