#ifndef SCORESYSTEM_HPP_INCLUDED
#define SCORESYSTEM_HPP_INCLUDED
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "../Receiver/Receiver.hpp"
#include "../Event/ScoreEvent.hpp"
#include "../EventManager/EventManager.hpp"
#include "../ResourceHolder/ResourceHolder.hpp"
#include <sstream>
#include <string>
namespace snake {
    class ScoreSystem : public Receiver<ScoreEvent>, public sf::Drawable, public sf::Transformable {
    public:
        ScoreSystem(sf::Vector2f size, EventManager& eventManager, const FontHolder* fonts);
        void update();
    private:
        void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    public:
        void receive(ScoreEvent& event) override;
    private:
        void addScores();
        std::string convertScoresToString();
    private:
        enum {SCORES_TO_ADD = 10};
        const FontHolder* textFont;
        sf::RenderTexture scoreArea;
        sf::Text textScores;
        unsigned scores;
        EventManager& mEventManager;
        sf::Sprite scoreSprite;
    };
}


#endif // SCORESYSTEM_HPP_INCLUDED
