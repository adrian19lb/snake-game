#ifndef ANIMATIONSYSTEM_HPP_INCLUDED
#define ANIMATIONSYSTEM_HPP_INCLUDED
#include "../Receiver/Receiver.hpp"
#include "../Event/AnimationEvent.hpp"
#include "../EventManager/EventManager.hpp"
#include "../Player/Player.hpp"
#include "../ResourceHolder/ResourceHolder.hpp"
#include <SFML/Graphics/Transform.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <memory>
namespace snake {
    class AnimationSystem : public Receiver<AnimationEvent> {
    public:
        AnimationSystem(Player* player, EventManager& eventManager, const TextureHolder& texture);
        void receive(AnimationEvent& animation) override;
    private:
        struct SpriteRect{
            sf::IntRect head = sf::IntRect(108, 0, 36, 36);
            sf::IntRect tail= sf::IntRect(0, 0, 36, 36);
            sf::IntRect middle = sf::IntRect(36, 0, 36, 36);
            sf::IntRect topLeft = sf::IntRect(0, 36, 36, 36);
            sf::IntRect topRight = sf::IntRect(36, 36, 36, 36);
            sf::IntRect bottomLeft = sf::IntRect(72, 36, 36, 36);
            sf::IntRect bottomRight = sf::IntRect(72, 0, 36, 36);
        };
        void rotate(AnimationEvent& event);
        void meshBasic();
        void meshCorners(AnimationEvent& event);
    private:
        EventManager& mEventManager;
        Player* mPlayer;
        const TextureHolder& mTextureHolder;
        SpriteRect mSpriteRect;
    };
}
#endif // ANIMATIONSYSTEM_HPP_INCLUDED
