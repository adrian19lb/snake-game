#ifndef TAILINCREMENTER_HPP_INCLUDED
#define TAILINCREMENTER_HPP_INCLUDED
#include "../SystemObserver/ISystemObserver.hpp"
namespace snake {
    class TailIncrementer : public ISystemObserver {
    public:
        void receive(CommandPtr& command);
    private:
        CommandPtr mIncrementCommand;
     };
}


#endif // TAILINCREMENTER_HPP_INCLUDED
