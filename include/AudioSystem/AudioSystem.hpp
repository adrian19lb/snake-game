#ifndef AUDIOSYSTEM_HPP_INCLUDED
#define AUDIOSYSTEM_HPP_INCLUDED
#include "../Receiver/Receiver.hpp"
#include <SFML/Audio/Sound.hpp>
#include "../Event/AudioEvent.hpp"
#include "../ResourceHolder/ResourceHolder.hpp"
#include "../EventManager/EventManager.hpp"
#include <list>
#include <memory>
namespace snake {
    class AudioSystem : public Receiver<AudioEvent> {
    public:
        AudioSystem(EventManager& eventManager, const SoundHolder& soundHolder);
        void receive(AudioEvent& audioEvent) override;
        void clearBuffer();
    private:
        void createSound(Audio::ID audioID);
    private:
        std::list<std::unique_ptr<sf::Sound>> mSoundsList;
        const SoundHolder& mSoundHolder;
        EventManager& mEventManager;
    };
}
#endif // AUDIOSYSTEM_HPP_INCLUDED
