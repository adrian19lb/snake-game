#ifndef SYSTEMSUBJECT_HPP_INCLUDED
#define SYSTEMSUBJECT_HPP_INCLUDED
#include <vector>
#include "../SystemObserver/ISystemObserver.hpp"
#include "../Command/Command.hpp"
namespace snake {
    class SystemSubject {
    public:
        void addObserver(ISystemObserver* observer) {
            mObservers.emplace_back(observer);
        }
        void removeObserver(ISystemObserver* observer) {
            auto found = std::find(mObservers.begin(), mObservers.end(), observer);
            if (found != mObservers.end()) {
                mObservers.erase(found);
            }
        }
    private:
        virtual void notify(CommandPtr command) = 0;
    protected:
        std::vector<ISystemObserver*> mObservers;
    };
}

#endif // SYSTEMSUBJECT_HPP_INCLUDED
