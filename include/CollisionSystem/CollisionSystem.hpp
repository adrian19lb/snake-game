#ifndef COLLISIONSYSTEM_HPP_INCLUDED
#define COLLISIONSYSTEM_HPP_INCLUDED
#include <SFML/Graphics/RenderWindow.hpp>
#include "../EventManager/EventManager.hpp"
#include "../Player/Player.hpp"
#include "../Item/ItemHolder.hpp"
#include "../SystemSubject/ISystemSubject.hpp"
#include "../Command/IncrementTailCommand.hpp"
#include "../Event/ItemSpawnEvent.hpp"
#include "../Event/AudioEvent.hpp"
#include "../Event/RequestStackEvent.hpp"
#include "../Event/ScoreEvent.hpp"
#include "../Receiver/Receiver.hpp"
namespace snake {
    class CollisionSystem : public SystemSubject, public Receiver<ItemSpawnEvent>   {
    public:
        ~CollisionSystem();
        CollisionSystem(sf::RenderWindow* gameWindow, EventManager& eventManager, Player* player, ItemHolder& holder);
        void check();
    private:
        bool isPlayerEatItem() const;
        bool isSpriteContainPoint(const sf::Sprite&  sprite, const sf::Vector2f& point) const;
        bool isItemSpawnInPlayer() const;
        bool isPlayerHeadImpactTail() const;
        void checkRequestToChangePosition();
        bool isReachedWinSize();
        void notify(CommandPtr command) override;
        void receive(ItemSpawnEvent& event) override;
    private:
        void sendPlayerTailIncrementNotification();
        template<typename EventType, typename... TArg>
        void sendNotification(TArg... arg) const {
            EventType event(arg... );
            mEventManager.emit(event);
        }
    private:
        EventManager& mEventManager;
        Player* mPlayer;
        ItemHolder& mItemHolder;
        sf::RenderWindow* mGameWindow;
    };
}


#endif // COLLISIONSYSTEM_HPP_INCLUDED
