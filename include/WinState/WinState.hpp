#ifndef INTROSTATE_HPP_INCLUDED
#define INTROSTATE_HPP_INCLUDED
#include "../IState/IState.hpp"
#include "../StateManager/StateManager.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <memory>
#include <vector>
#include "../Button/Button.hpp"
namespace snake {
    class WinState : public IState {
    public:
        WinState(StateManager& stateManager, GameContext context);
        void draw() override;
        void handleInput(sf::Event& event) override;
        void update(sf::Time dt) override;
    private:
        std::vector<std::unique_ptr<Button>> mButtons;

    };
}


#endif // INTROSTATE_HPP_INCLUDED
