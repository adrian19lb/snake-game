#ifndef BASERECEIVER_HPP_INCLUDED
#define BASERECEIVER_HPP_INCLUDED
#include "GeneratorId.hpp"
namespace snake {
    class BaseReceiver {
    public:
        BaseReceiver();
        unsigned getId();
    private:
        unsigned mId;
    };
}
#endif // BASERECEIVER_HPP_INCLUDED
