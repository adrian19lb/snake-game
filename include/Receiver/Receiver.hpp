#ifndef RECEIVER_HPP_INCLUDED
#define RECEIVER_HPP_INCLUDED
#include "BaseReceiver.hpp"
namespace snake {
    template<typename EventType>
    class Receiver : public BaseReceiver {
    public:
        virtual ~Receiver() { }
        virtual void receive(EventType& event) = 0;
    };
}
#endif // RECEIVER_HPP_INCLUDED
