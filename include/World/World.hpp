#ifndef WORLD_HPP_INCLUDED
#define WORLD_HPP_INCLUDED
#include "../IState/IState.hpp"
#include "../Player/Player.hpp"
#include "../Item/Item.hpp"
#include "../Map/Map.hpp"
#include "../Item/ItemHolder.hpp"
#include "../EventManager/EventManager.hpp"
#include "../CollisionSystem/CollisionSystem.hpp"
#include "../Command/IncrementTailCommand.hpp"
#include "../InputSystem/InputSystem.hpp"
#include "../TailIncrementer/TailIncrementer.hpp"
#include "../AnimationSystem/AnimationSystem.hpp"
#include "../MovementSystem/MovementSystem.hpp"
#include "../AudioSystem/AudioSystem.hpp"
#include "../Item/ItemSpawner.hpp"
#include "../Event/RequestStackEvent.hpp"
#include "../ScoreSystem/ScoreSystem.hpp"
#include "../PlayerTeleporter/PlayerTeleporter.hpp"
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>
#include "../Receiver/Receiver.hpp"
#include <memory>
#include <deque>
namespace snake {
    class World : public sf::Drawable, public Receiver<RequestStackEvent> {
    public:
        ~World();
        World(IState::GameContext gameContext);
        void processInput(sf::Event& event);
        void processCollision();
        void updateAudio();
        void processMovement();
        void updateScores();
        void checkRequestPlayerTeleporter();
        RequestStackEvent checkRequest();
    private:
        void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
        void receive(RequestStackEvent& event) override;
    private:
        IState::GameContext mGameContext;
        std::unique_ptr<Player> mPlayer;
        Map mGameMap;
        EventManager mEventManager;
        AudioSystem mAudioSystem;
        ItemHolder mItemHolder;
        InputSystem mPlayerInput;
        CollisionSystem mCollisionDetector;
        TailIncrementer mTailIncrementer;
        AnimationSystem mPlayerAnimator;
        MovementSystem mPlayerMovement;
        ItemSpawner mItemSpawner;
        ScoreSystem mScores;
        PlayerTeleporter teleporter;
    private:
        std::deque<RequestStackEvent> mRequestQueue;
    };
}


#endif // WORLD_HPP_INCLUDED
