#ifndef PAUSESTATE_HPP_INCLUDED
#define PAUSESTATE_HPP_INCLUDED
#include "../IState/IState.hpp"
#include "../StateManager/StateManager.hpp"
#include <SFML/Window/Keyboard.hpp>
#include "../Button/Button.hpp"
#include <vector>
#include <memory>
namespace snake {
    class PauseState: public IState {
    public:
        PauseState(StateManager& stateManager, GameContext context);
        void handleInput(sf::Event& event) override;
        void update(sf::Time dt) override;
        void draw() override;
    private:
        std::vector<std::unique_ptr<Button>> mButtons;
    };
}
#endif // PAUSESTATE_HPP_INCLUDED
