#ifndef RESOURCEHOLDER_HPP_INCLUDED
#define RESOURCEHOLDER_HPP_INCLUDED
#include <map>
#include <memory>
#include <string>
#include <cassert>
#include <exception>
#include <typeindex>
#include "../ResourceIdentifier/ResourceIdentifier.hpp"
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Graphics/Font.hpp>
namespace snake {
    template<typename Identifier, typename Resource>
    class ResourceHolder {
    public:
        using ResourcePtr = std::unique_ptr<Resource>;
    public:
        void load(Identifier id, const std::string& filename);
        template<typename Parameter>
        void load(Identifier id, const std::string& filename, Parameter parameter);
        Resource& get(Identifier id);
        const Resource& get(Identifier id) const;
    private:
        std::map<Identifier, ResourcePtr> mResourceMap;
    };

    template<typename Identifier>
    class ResourceHolder <Identifier, sf::Music> {
    public:
        using MusicPtr = std::unique_ptr<sf::Music>;
    public:
        void load(Identifier id, const std::string& filename);
        sf::Music& get(Identifier id);
        void mute();
        void unmute();
        const sf::Music& get(Identifier id) const;
    private:
        std::map<Identifier, MusicPtr> mMusicMap;
    };
    using MusicHolder = ResourceHolder<States::ID, sf::Music>;
    using TextureHolder = ResourceHolder<Entity::ID, sf::Texture>;
    using SoundHolder = ResourceHolder<Audio::ID, sf::SoundBuffer>;
    using FontHolder = ResourceHolder<Entity::ID, sf::Font>;
}
#include "../../src/ResourceHolder/ResourceHolder.inl"
#endif // RESOURCEHOLDER_HPP_INCLUDED
