#ifndef CONSTANTS_HPP_INCLUDED
#define CONSTANTS_HPP_INCLUDED

namespace snake {

    const unsigned SCREEN_WIDTH = 828;
    const unsigned SCREEN_HEIGHT = 612;
    const float MAP_TITLE_WIDTH = 36;
    const float MAP_TITLE_HEIGHT = 36;
    const unsigned MAP_TITLES_NUMBER = (SCREEN_HEIGHT / MAP_TITLE_HEIGHT) * (SCREEN_WIDTH / MAP_TITLE_WIDTH);
    const float TIME_STEP = 1.f / 10.f;
}

#endif // CONSTANTS_HPP_INCLUDED
