#ifndef ISTATE_HPP_INCLUDED
#define ISTATE_HPP_INCLUDED
#include <SFML/System/Time.hpp>
#include <memory>
#include "../ResourceHolder/ResourceHolder.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <vector>
namespace snake {
    class StateManager;
    class IState {
    public:
        struct GameContext {
            GameContext(sf::RenderWindow* window, TextureHolder* texture, SoundHolder* sound, MusicHolder* music, FontHolder* font);
            sf::RenderWindow* gameWindow;
            TextureHolder* textures;
            SoundHolder* sounds;
            FontHolder* fonts;
            MusicHolder* musics;
        };
    public:
        IState(StateManager& stateManager, GameContext context);
        ~IState() { }
        virtual void draw() = 0;
        virtual void update(sf::Time dt) = 0;
        virtual void handleInput(sf::Event& event) = 0;
    protected:
        StateManager* mStateManager;
        GameContext mGameContext;
        sf::Sprite mBackground;
    };
    using StatePtr = std::unique_ptr<IState>;

}


#endif // ISTATE_HPP_INCLUDED
