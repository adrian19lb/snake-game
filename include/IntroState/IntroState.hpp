#ifndef INTROSTATE_HPP_INCLUDED
#define INTROSTATE_HPP_INCLUDED
#include "../IState/IState.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
namespace snake {
    class IntroState : public IState {
    public:
        IntroState(StateManager& stateManager, GameContext context);
        void draw() override;
        void handleInput(sf::Event& event) override;
        void update(sf::Time dt) override;
    };
}


#endif // INTROSTATE_HPP_INCLUDED
