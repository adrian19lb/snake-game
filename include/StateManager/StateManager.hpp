#ifndef STATEMANAGER_HPP_INCLUDED
#define STATEMANAGER_HPP_INCLUDED
#include "../IState/IState.hpp"
#include <memory>
#include <vector>
#include <map>
#include <functional>
#include <cassert>
namespace snake {
    class StateManager {
    public:
        enum Action {
            Push,
            Pop,
            Clear,
            None
        };
    private:
        struct PendingChanges {
            explicit PendingChanges(Action action, States::ID stateID = States::None);
            Action mAction;
            States::ID mStateID;
        };
    public:
        StateManager(IState::GameContext context);
        void pushState(States::ID stateID);
        void popState();
        IState* top() const;
        bool isEmpty() const;
        void clearStates();
        unsigned size() const;
        StatePtr createState(States::ID stateID);
        void applyPendingChanges();
        template<typename StateType>
        void registerState(States::ID stateID) {
            mStatesFactory[stateID] = [this]()->StatePtr {
                return std::move(std::make_unique<StateType>(*this, mGameContext));
            };
        }
    private:
        std::vector<StatePtr> mStatesStack;
        std::vector<PendingChanges> mPendingList;
        std::map<States::ID, std::function<StatePtr()>> mStatesFactory;
        IState::GameContext mGameContext;
    };
}
#endif // STATEMANAGER_HPP_INCLUDED
