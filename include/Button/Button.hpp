#ifndef BUTTON_HPP_INCLUDED
#define BUTTON_HPP_INCLUDED

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Mouse.hpp>
#include <string>
#include <memory>
#include "../ResourceHolder/ResourceHolder.hpp"
#include "../IState/IState.hpp"
#include "BasicButtonComponent.hpp"

namespace snake {

    class Button : public sf::Transformable {
    public:
        Button(IState::GameContext* gameContext);
        ~Button() { }
    public:
        void create(const sf::Vector2u& size, const std::string& internalText, sf::Color textColor = sf::Color::White);
        void setTexture(const sf::Texture& texture);
        bool isPressed();
        bool isSelected();
        void draw();
    private:
        void playSound();
        void update();
    private:
        IState::GameContext* mGameContext;
        ButtonComponents components;
        sf::Sprite sprite;
        bool mIsButtonClick;
        bool mIsButtonSelected;
        bool mIsButtonSoundPlayed;
    };
}


#endif // BUTTON_HPP_INCLUDED
