#ifndef BASICBUTTONCOMPONENT_HPP_INCLUDED
#define BASICBUTTONCOMPONENT_HPP_INCLUDED

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include "../ResourceHolder/ResourceHolder.hpp"
#include <SFML/Audio/Sound.hpp>
#include <string>

namespace snake {

    class ButtonComponents {
    private:
        struct ModelComponent{
            sf::Sprite texture;
            sf::Text text;
        };
        struct SoundComponent {
            sf::Sound select;
            sf::Sound click;
        };
        struct TextColorComponent {
            sf::Color basic;
            sf::Color repainted;
        };
    public:
        ~ButtonComponents();
        ButtonComponents(FontHolder* fontsHolder, SoundHolder* soundsHolder);
        void render();
        void paint();
        void repaint();
        void create(const sf::Vector2u& size, const std::string& internalText, sf::Color textColor);
    private:
        void setText(const sf::Vector2u& size, const std::string& internalText, sf::Color textColor);
        void setSounds();
    public:
        const sf::Texture& getTexture() const;
        void playClickSound();
        void playSelectSound();
        void setTexture(const sf::Texture& texture);
    private:
        sf::RenderTexture renderTexture;
        ModelComponent models;
        TextColorComponent textColors;
        SoundComponent sounds;
        FontHolder* mFontsHolder;
        SoundHolder* mSoundsHolder;
    };

}

#endif // BASICBUTTONCOMPONENT_HPP_INCLUDED
