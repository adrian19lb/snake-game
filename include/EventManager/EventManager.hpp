#ifndef EVENTMENAGER_HPP_INCLUDED
#define EVENTMENAGER_HPP_INCLUDED
#include <vector>
#include "../Receiver/Receiver.hpp"
namespace snake {
    class EventManager {
    public:
        using ReceiverArray = std::vector<BaseReceiver*>;
        using ImplSubscriberArray = std::vector<ReceiverArray>;
    public:
        ~EventManager() { }
        EventManager() { }
        template<typename EventType, typename ReceiverType> void subscribe(ReceiverType* receiver);
        template<typename EventType> void emit(EventType& event);
        template<typename EventType> void unsubscribe(BaseReceiver* receiver);
    private:
        ImplSubscriberArray mSubscriber;
    };
}
#include "../../src/EventManager/EventManager.inl"
#endif // EVENTMENAGER_HPP_INCLUDED
