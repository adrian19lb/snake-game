#ifndef MENUSTATE_HPP_INCLUDED
#define MENUSTATE_HPP_INCLUDED

#include "../IState/IState.hpp"
#include "../StateManager/StateManager.hpp"
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window/Mouse.hpp>
#include "../Button/Button.hpp"
#include <vector>
#include <memory>

namespace snake {
    class MenuState : public IState {
    public:
        ~MenuState();
        MenuState(StateManager& stateManager, GameContext context);
    private:
        void createButtons();
        void setButtonsGlobalPosition();
        void playAndLoopMusic();
        void setMuteOrUnmuteButtonTexture();
    public:
        void handleInput(sf::Event& event) override;
    private:
        void activePlayButtonAction();
        void activeCreditsButtonAction();
        void activeQuitButtonsAction();
        void activeMuteButtonAction();
        void activeUnMuteButtonAction();
    public:
        void update(sf::Time dt) override;
        void draw() override;
    private:
        enum {PLAY = 0, CREDITS = 1, QUIT = 2, MUTE = 3, BUTTONS_AMOUNT = 4};
        std::vector<std::unique_ptr<Button>> menuButtons;
    };
}

#endif // MENUSTATE_HPP_INCLUDED
