#ifndef PLAYERTELEPORTER_HPP_INCLUDED
#define PLAYERTELEPORTER_HPP_INCLUDED
#include "../Player/Player.hpp"
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "../Constants/Constants.hpp"

namespace snake {
    class PlayerTeleporter {
    public:
        ~PlayerTeleporter();
        PlayerTeleporter(Player* player);
        void checkChangePlayerPosition();
    private:
        Player* mPlayer;
    };
}


#endif // PLAYERTELEPORTER_HPP_INCLUDED
