#ifndef ITEM_HPP_INCLUDED
#define ITEM_HPP_INCLUDED
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "../ResourceHolder/ResourceHolder.hpp"
#include <memory>
namespace snake {
    class Item : public sf::Drawable {
    public:
        Item(sf::Vector2f coordinates, const TextureHolder& holder);
        sf::Sprite& getSprite();
        const Entity::ID getID();
    private:
        void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    private:
        sf::Sprite mSprite;
        Entity::ID mID;
    };
}


#endif // ITEM_HPP_INCLUDED
