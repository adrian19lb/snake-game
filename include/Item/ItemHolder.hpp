#ifndef ITEMHOLDER_HPP_INCLUDED
#define ITEMHOLDER_HPP_INCLUDED
#include "../Receiver/Receiver.hpp"
#include "ItemSpawner.hpp"
#include "Item.hpp"
#include "../Event/ItemSpawnEvent.hpp"
#include "../EventManager/EventManager.hpp"
#include <memory>
namespace snake {
    class ItemHolder : public Receiver<ItemSpawnEvent> {
    public:
        ItemHolder(EventManager& eventManager);
        void receive(ItemSpawnEvent& event) override;
        void push(std::unique_ptr<Item> item);
        Item& getItem() const;
    private:
        EventManager& mEventManager;
        std::unique_ptr<Item> mItem;
    };
}
#endif // ITEMHOLDER_HPP_INCLUDED

