#ifndef ITEMSPAWNER_HPP_INCLUDED
#define ITEMSPAWNER_HPP_INCLUDED
#include "Item.hpp"
#include "../EventManager/EventManager.hpp"
#include "../Receiver/Receiver.hpp"
#include "../Event/CollisionEvent.hpp"
#include "../Player/Player.hpp"
#include "../ResourceHolder/ResourceHolder.hpp"
#include <memory>
#include <random>
#include <chrono>
#include <functional>
#include <vector>
#include "../Event/ItemSpawnEvent.hpp"
namespace snake {
    class ItemSpawner : public Receiver<CollisionEvent> {
    public:
        ItemSpawner(EventManager& eventManager, const TextureHolder& texture);
        void receive(CollisionEvent& event) override;
    public:
        void makeRandom();
    private:
        EventManager& mEventManager;
        const TextureHolder& mTextureHolder;
    };
}
#endif // ITEMSPAWNER_HPP_INCLUDED
