#include "../../include/IState/IState.hpp"
namespace snake {
    IState::GameContext::GameContext(sf::RenderWindow* window, TextureHolder* texture, SoundHolder* sound, MusicHolder* music, FontHolder* font)
    : gameWindow(window)
    , textures(texture)
    , sounds(sound)
    , musics(music)
    , fonts (font) {
    }
    IState::IState(StateManager& stateManager, GameContext context)
    : mStateManager(&stateManager)
    , mGameContext(context)
    , mBackground() {
    }
}
