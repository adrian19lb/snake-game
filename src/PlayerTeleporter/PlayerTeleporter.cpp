#include "../../include/PlayerTeleporter/PlayerTeleporter.hpp"
namespace snake {

    PlayerTeleporter::~PlayerTeleporter() {
    }

    PlayerTeleporter::PlayerTeleporter(Player* player)
    : mPlayer(player) {
    }

    void PlayerTeleporter::checkChangePlayerPosition() {
        sf::Sprite& headSprite = mPlayer->getHead().mSprite;
        const sf::FloatRect& headGlobalBounds = headSprite.getGlobalBounds();
        const sf::Vector2f& currentHeadPosition = headSprite.getPosition();

        sf::Vector2f newHeadPosition;

        if (headGlobalBounds.contains(currentHeadPosition.x, - (MAP_TITLE_HEIGHT / 2) )) {
            newHeadPosition.x = currentHeadPosition.x;
            newHeadPosition.y = SCREEN_HEIGHT - (MAP_TITLE_HEIGHT / 2);
        }

        else if (headGlobalBounds.contains(currentHeadPosition.x, SCREEN_HEIGHT + (MAP_TITLE_HEIGHT / 2) )) {
            newHeadPosition.x = currentHeadPosition.x;
            newHeadPosition.y = MAP_TITLE_HEIGHT / 2;
        }

        else if (headGlobalBounds.contains( - MAP_TITLE_WIDTH / 2, currentHeadPosition.y)) {
            newHeadPosition.x = SCREEN_WIDTH - (MAP_TITLE_WIDTH / 2);
            newHeadPosition.y = currentHeadPosition.y;
        }

        else if (headGlobalBounds.contains(SCREEN_WIDTH, currentHeadPosition.y)) {
            newHeadPosition.x = MAP_TITLE_WIDTH / 2;
            newHeadPosition.y = currentHeadPosition.y;
        }

        else {
            newHeadPosition = sf::Vector2f(currentHeadPosition);
        }

        headSprite.setPosition(newHeadPosition);
    }
}
