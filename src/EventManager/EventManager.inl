namespace snake {
    template<typename EventType, typename ReceiverType>
    void EventManager::subscribe(ReceiverType* receiver) {
        unsigned receiverID = receiver->getId();
        unsigned eventID = EventType::getID();
        if (mSubscriber.size() <= eventID) {
            mSubscriber.resize(eventID + 1);
        }
        auto& receivers = mSubscriber[eventID];
        if (receivers.size() <= receiverID) {
            receivers.resize(receiverID + 1);
        }
        receivers[receiverID] = receiver;
    }
    template<typename EventType>
    void EventManager::emit(EventType& event) {
        auto& receivers = mSubscriber.at(event.getID());
        for (auto receiver : receivers) {
            if (receiver) {
                static_cast<Receiver<EventType>&>(*receiver).receive(event);
            }
        }
    }
}
