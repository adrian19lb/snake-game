#include "../../include/ScoreSystem/ScoreSystem.hpp"
namespace snake {

    ScoreSystem::ScoreSystem(sf::Vector2f size, EventManager& eventManager, const FontHolder* fonts)
    : textFont(fonts)
    , scoreArea()
    , textScores("SCORES: 0", textFont->get(Entity::Button))
    , scores(0u)
    , mEventManager(eventManager)
    , scoreSprite() {
        scoreArea.create(size.x, size.y);
        mEventManager.subscribe<ScoreEvent>(this);
        textScores.setCharacterSize(0.5 * size.y);
        textScores.setPosition(5.f, 5.f);
    }

    void ScoreSystem::update() {
        scoreArea.clear(sf::Color(50, 50, 50, 50));
        scoreArea.draw(textScores);
        scoreArea.display();
        scoreSprite.setTexture(scoreArea.getTexture());
    }

    void ScoreSystem::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        target.draw(scoreSprite, states);
    }

    void ScoreSystem::receive(ScoreEvent& event) {
        addScores();
        std::string newScores = convertScoresToString();
        textScores.setString(newScores);
    }

    void ScoreSystem::addScores() {
        scores += SCORES_TO_ADD;
    }

    std::string ScoreSystem::convertScoresToString() {
        std::string text("SCORES:  ");
        std::stringstream sstream;
        sstream << "SCORES: " << scores;
        return sstream.str();
    }
}
