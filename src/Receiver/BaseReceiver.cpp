#include "../../include/Receiver/BaseReceiver.hpp"
namespace snake {
    BaseReceiver::BaseReceiver() {
        mId = generateUniqueReceiverId();
    }
    unsigned BaseReceiver::getId() {
        return mId;
    }
}
