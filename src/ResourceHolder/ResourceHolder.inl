namespace snake {
    template<typename Identifier, typename Resource>
    void ResourceHolder<Identifier, Resource>::load(Identifier id, const std::string& filename) {
        std::unique_ptr<Resource> newResource(std::make_unique<Resource>());
        if (!newResource->loadFromFile(filename)) {
            throw std::runtime_error("ResourceHolder::load - failed to load: " + filename);
        }
        auto inserted = mResourceMap.emplace(std::make_pair(id, std::move(newResource)));
        assert(inserted.second);
    }
    template<typename Identifier, typename Resource>
    template<typename Parameter>
    void ResourceHolder<Identifier, Resource>::load(Identifier id, const std::string& filename, Parameter parameter) {
        std::unique_ptr<Resource> newResource(std::make_unique<Resource>());
        if (!newResource->loadFromFile(filename, parameter)) {
            throw std::runtime_error("ResourceHolder::load - failed to load: " + filename);
        }
        auto inserted = mResourceMap.emplace(std::make_pair(id, std::move(newResource)));
        assert(inserted.second);
    }
    template<typename Identifier, typename Resource>
    Resource& ResourceHolder<Identifier, Resource>::get(Identifier id) {
        auto found = mResourceMap.find(id);
        assert(found != mResourceMap.end());
        return *found->second;
    }
    template<typename Identifier, typename Resource>
    const Resource& ResourceHolder<Identifier, Resource>::get(Identifier id) const {
        auto found = mResourceMap.find(id);
        assert(found != mResourceMap.end());
        return *found->second;
    }

    template<typename Identifier>
    void ResourceHolder<Identifier, sf::Music>::load(Identifier id, const std::string& filename) {
        std::unique_ptr<sf::Music> newResource(std::make_unique<sf::Music>());
        if (!newResource->openFromFile(filename)) {
            throw std::runtime_error("ResourceHolder::load - failed to load: " + filename);
        }
        auto inserted = mMusicMap.emplace(std::make_pair(id, std::move(newResource)));
        assert(inserted.second);
    }
    template<typename Identifier>
    sf::Music& ResourceHolder<Identifier, sf::Music>::get(Identifier id) {
        auto found = mMusicMap.find(id);
        assert(found != mMusicMap.end());
        return *found->second;
    }
    template<typename Identifier>
    const sf::Music& ResourceHolder<Identifier, sf::Music>::get(Identifier id) const {
        auto found = mMusicMap.find(id);
        assert(found != mMusicMap.end());
        return *found->second;
    }
    template<typename Identifier>
    void ResourceHolder<Identifier, sf::Music>::mute() {
        for (auto & music : mMusicMap) {
            music.second->setVolume(0.f);
        }
    }
    template<typename Identifier>
    void ResourceHolder<Identifier, sf::Music>::unmute() {
        for (auto & music : mMusicMap) {
            music.second->setVolume(100.f);
        }
    }


}
