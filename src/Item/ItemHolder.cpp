#include "../../include/Item/ItemHolder.hpp"
namespace snake {
    ItemHolder::ItemHolder(EventManager& eventManager)
    : mEventManager(eventManager)
    , mItem(nullptr) {
        mEventManager.subscribe<ItemSpawnEvent>(this);
    }
    void ItemHolder::receive(ItemSpawnEvent& event) {
        if (mItem) {
            mItem.reset();
        }
        push(std::move(event.getItem()));
    }
    void ItemHolder::push(std::unique_ptr<Item> item) {
        mItem = std::move(item);
    }
    Item& ItemHolder::getItem() const {
        return *mItem;
    }
}
