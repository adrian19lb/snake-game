#include "../../include/Item/Item.hpp"
namespace snake {
    Item::Item(sf::Vector2f coordinates, const TextureHolder& holder)
    : mID(Entity::Apple) {
        mSprite.setPosition(coordinates);
        mSprite.setTexture(holder.get(Entity::Apple));
    }
    void Item::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        target.draw(mSprite);
    }
    sf::Sprite& Item::getSprite() {
        return mSprite;
    }
    const Entity::ID Item::getID() {
        return mID;
    }
}
