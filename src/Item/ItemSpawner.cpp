#include "../../include/Item/ItemSpawner.hpp"
namespace snake {
    ItemSpawner::ItemSpawner(EventManager& eventManager, const TextureHolder& texture)
    : mEventManager(eventManager)
    , mTextureHolder(texture) {
        mEventManager.subscribe<CollisionEvent>(this);
        makeRandom();
    }
    void ItemSpawner::receive(CollisionEvent& event) {
        auto e1 = event.getEntities().first;
        auto e2 = event.getEntities().second;
        if (e1 == Entity::Player && e2 == Entity::Apple) {
            makeRandom();
        }
        else if (e1 == Entity::Apple && e2 == Entity::Player) {
            makeRandom();
        }
    }
    void ItemSpawner::makeRandom() {
        unsigned lowerRange = 0u;
        unsigned upperRangeX = 22u;
        unsigned upperRangeY = 16u;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine generator(seed);
        std::uniform_int_distribution<unsigned> distributionX(lowerRange, upperRangeX);
        std::uniform_int_distribution<unsigned> distributionY(lowerRange, upperRangeY);
        unsigned randomX = distributionX(generator);
        unsigned randomY = distributionY(generator);
        sf::Vector2f coordinates(randomX * 36.f, randomY * 36.f);
        std::unique_ptr<Item> newItem(std::make_unique<Item>(coordinates, mTextureHolder));
        ItemSpawnEvent event(std::move(newItem));
        mEventManager.emit(event);
    }
}
