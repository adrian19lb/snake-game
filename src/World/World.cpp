#include "../../include/World/World.hpp"
namespace snake {
    World::~World() {
    }
    World::World(IState::GameContext gameContext)
    : mGameContext(gameContext)
    , mPlayer(std::make_unique<Player>(sf::Vector2f(mGameContext.gameWindow->getSize().x/2, mGameContext.gameWindow->getSize().y/2), *mGameContext.textures))
    , mGameMap(*mGameContext.textures)
    , mEventManager()
    , mAudioSystem(mEventManager, *gameContext.sounds)
    , mItemHolder(mEventManager)
    , mItemSpawner(mEventManager, *mGameContext.textures)
    , mPlayerInput(*mGameContext.gameWindow, mEventManager)
    , mCollisionDetector(mGameContext.gameWindow, mEventManager, mPlayer.get(), mItemHolder)
    , mTailIncrementer()
    , mPlayerAnimator(mPlayer.get(), mEventManager, *mGameContext.textures)
    , mPlayerMovement()
    , mScores(sf::Vector2f(160, 30), mEventManager, mGameContext.fonts)
    , teleporter(mPlayer.get()) {
        mGameMap.create( { 36u, 36u });
        mPlayerInput.addObserver(&mPlayerMovement);
        mCollisionDetector.addObserver(&mTailIncrementer);
        mEventManager.subscribe<RequestStackEvent>(this);
        mScores.setPosition(mGameContext.gameWindow->getSize().x - 175.f, 8.f);
    }
    void World::receive(RequestStackEvent& event) {
        mRequestQueue.emplace_back(event);
    }
    void World::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        target.draw(mGameMap);
        target.draw(mItemHolder.getItem());
        target.draw(*mPlayer);
        target.draw(mScores);
    }
    void World::processInput(sf::Event& event) {
        mPlayerInput.process(mPlayer.get(), event);
    }
    void World::processMovement() {
        mPlayerMovement.process();
    }
    void World::processCollision() {
        mCollisionDetector.check();
    }
    void World::updateAudio() {
        mAudioSystem.clearBuffer();
    }
    void World::updateScores() {
        mScores.update();
    }
    void World::checkRequestPlayerTeleporter() {
        teleporter.checkChangePlayerPosition();
    }
    RequestStackEvent World::checkRequest() {
        RequestStackEvent freshRequest;
        if (!mRequestQueue.empty()) {
            freshRequest = mRequestQueue.front();
            mRequestQueue.pop_front();
        }
        return RequestStackEvent(freshRequest);
    }
}
