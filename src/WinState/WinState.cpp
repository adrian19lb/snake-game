#include "../../include/WinState/WinState.hpp"
namespace snake {
    WinState::WinState(StateManager& stateManager, GameContext context)
    : IState(stateManager, context) {
        mBackground.setTexture(mGameContext.textures->get(Entity::Win));
        sf::Vector2u buttonSize(350u, 75u);
        sf::Vector2f buttonPosition(mGameContext.gameWindow->getSize());
        for (unsigned i = 0; i < 2u; ++i) {
            mButtons.emplace_back(std::make_unique<Button>(&mGameContext));
        }
        mButtons[0]->create(buttonSize, "RETRY");
        mButtons[1]->create(buttonSize, "BACK TO MENU");

        mButtons[0]->setPosition(buttonPosition.x/2 - 150u, buttonPosition.y - 250);
        mButtons[1]->setPosition(buttonPosition.x/2 - 150u, buttonPosition.y - 175);

    }
    void WinState::draw() {
        mGameContext.gameWindow->draw(mBackground);
        for (auto& button : mButtons) {
            button->draw();
        }
    }
    void WinState::handleInput(sf::Event& event) {
        if (mButtons[0]->isSelected() && mButtons[0]->isPressed()) {
            mStateManager->popState();
            mStateManager->popState();
            mStateManager->pushState(States::Playing);
        }
        else if (mButtons[1]->isSelected() && mButtons[1]->isPressed()) {
            mStateManager->popState();
            mStateManager->popState();
        }
    }
    void WinState::update(sf::Time dt) {
    }
}
