namespace snake {
    template<typename TType>
    ActionMap<TType>::ActionMap() :
        mActionBuffer(),
        mIsActionRepeated(false) {
    }
    template<typename TType>
    void ActionMap<TType>::insert(const TType& identifier, ActionPtr action) {
        mActionMap.emplace(std::make_pair(identifier, std::move(action)));
    }
    template<typename TType>
    void ActionMap<TType>::erase(const TType& identifier) {
        auto found = mActionMap.find(identifier);
        if (found != mActionMap.end()) {
            mActionMap.erase(found);
        }
    }
    template<typename TType>
    const Action& ActionMap<TType>::get(const TType& identifier) const {
        auto found = mActionMap.find(identifier);
        assert(found != mActionMap.end());
        return *found->second;
    }
    template<typename TType>
    const Action& ActionMap<TType>::operator[](const TType& identifier) const {
        auto found = mActionMap.find(identifier);
        assert(found != mActionMap.end());
        return *found->second;
    }
    template<typename TType>
    bool ActionMap<TType>::isActive(const TType& identifier) {
        if (mCurrentAction == identifier) {
            return true;
        }
        else if (mCurrentAction != identifier) {
            return false;
        }
    }
    template<typename TType>
    void ActionMap<TType>::checkEvent() {
        for (auto& action : mActionMap) {
            if (action.second->check()) {
                mCurrentAction = action.first;
                break;
            }
            else {
                mPreviousAction = mCurrentAction;
            }
        }
    }
    template<typename TType>
    const TType& ActionMap<TType>::getCurrentAction() {
        return mCurrentAction;
    }
    template<typename TType>
    const TType& ActionMap<TType>::getPreviousAction() {
        return mPreviousAction;
    }
    template<typename TType>
    void ActionMap<TType>::setMovementActionRepeated(bool isKeyRepeated) {
        mIsActionRepeated = isKeyRepeated;
    }
    template<typename TType>
    void ActionMap<TType>::clearEvents() {
    }
}
