namespace snake {
    template<typename TBuffer>
    void ActionX::checkBufferSize(ActionTypeX action) {
        if (std::type_index(typeid(TBuffer)) == std::type_index(typeid(KeyboardBuffer))) {
            if (mKeyboardBuffer.size() <= action) {
                mKeyboardBuffer.resize(action + 1);
            }
        }
        else if (std::type_index(typeid(TBuffer)) == std::type_index(typeid(MouseBuffer))) {
            if (mMouseBuffer.size() <= action) {
                mMouseBuffer.resize(action + 1);
            }
        }
    }
}

