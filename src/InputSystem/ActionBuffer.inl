namespace snake {
    template<typename TData>
    ActionBuffer<TData>::ActionBuffer(unsigned bufferSize) :
        mBufferSize(bufferSize),
        mHead(nullptr),
        mCount(0u) {
    }
    template<typename TData>
    ActionBuffer<TData>::~ActionBuffer() {
        while (mHead) {
            pop();
        }
    }
    template<typename TData>
    void ActionBuffer<TData>::push(const TData& action) {
        mCount++;
        Node* newNode = new Node;
        newNode->data = action;
        newNode->next = mHead;
        mHead = newNode;
    }
    template<typename TData>
    void ActionBuffer<TData>::pop() {
        Node* node;
        node = mHead;
        if (node) {
            mHead = node->next;
            delete node;
            mCount--;
        }
    }
    template<typename TData>
    const typename ActionBuffer<TData>::Node* ActionBuffer<TData>::top() {
        if (!isEmpty()) {
            return mHead;
        }
        else {
            return nullptr;
        }
    }
    template<typename TData>
    bool ActionBuffer<TData>::isEmpty() {
        return !mHead;
    }
    template<typename TData>
    void ActionBuffer<TData>::popBack() {
        Node* node = mHead;
        if (node) {
            mCount--;
            if (node->next) {
                while (node->next->next) {
                    node = node->next;
                }
                delete node->next;
                node->next = nullptr;
            }
            else {
                delete node;
                mHead = nullptr;
            }
        }
    }
    template<typename TData>
    void ActionBuffer<TData>::clearBuffer() {
        while (mBufferSize < mCount) {
            popBack();
        }
    }
    template<typename TData>
    bool ActionBuffer<TData>::isActive(const TData& action) {
        if (mHead) {
            if (mHead->data == action) {
                return true;
            }
            else
                return false;
        }
        else {
            return false;
        }
    }
}
