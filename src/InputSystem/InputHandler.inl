namespace snake {
    template<typename TType>
    InputHandler<TType>::InputHandler() :
        mKeyboard(),
        mMouse(),
        mCurrentAction(),
        mPreviousAction() {
    }
    template<typename TType>
    bool InputHandler<TType>::checkEvents(const TType& identifier, const Keybord::Event& keyEvent, const Mouse::Event& buttonEvent) {
        bool isEventOccured;
        isEventOccured = isEventDetected<Keybord>(identifier, keyEvent);
        if (isEventOccured) {
            return isEventOccured;
        }
        else if (!isEventOccured) {
            isEventOccured = isEventDetected<Mouse>(identifier, buttonEvent);
            return isEventOccured;
        }
    }
    template<typename TType>
    template<typename InputType>
    bool InputHandler<TType>::isEventDetected(const TType& identifier, const typename InputType::Event& event) {
        if (mActionMap[identifier].get().first == event.type && mActionMap[identifier].get().second == event.code) {
            mPreviousAction = mCurrentAction;
            mCurrentAction = identifier;
            return true;
        }
        else {
            return false;
        }
    }
    template<typename TType>
    bool InputHandler<TType>::isActive(const TType& identifier) {
        Keybord::Event keyboardKeyEvent;
        Mouse::Event mouseButtonEvent;
        bool isEventDetected;
        keyboardKeyEvent = Keybord::Event(mKeyboard.createEvent());
        mouseButtonEvent = Mouse::Event(mMouse.createEvent());
        isEventDetected = checkEvents(identifier, keyboardKeyEvent, mouseButtonEvent);
        return isEventDetected;
    }
    template<typename TType>
    void InputHandler<TType>::pushAction(const TType& identifier, ActionPtr action) {
        mActionMap.insert(identifier, std::move(action));
    }
    template<typename TType>
    const TType& InputHandler<TType>::getCurrentAction() {
        return mCurrentAction;
    }
    template<typename TType>
    const TType& InputHandler<TType>::getPreviousAction() {
        return mPreviousAction;
    }
}
