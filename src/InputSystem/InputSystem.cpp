#include "../../include/InputSystem/InputSystem.hpp"
namespace snake {
    InputSystem::InputSystem(sf::RenderWindow& window, EventManager& eventManager)
    : mGameWindow(window)
    , mEventManager(eventManager)
    , mDirection(Direction::Vertical) {
    }
    void InputSystem::notify(CommandPtr command) {
        for (const auto& observer : mObservers) {
            observer->receive(command);
        }
    }
    void InputSystem::createAction() {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && mDirection == Vertical) {
            mCurrentAction = "moveUp";
            AudioEvent audioEvent(Audio::Click);
            mEventManager.emit(audioEvent);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && mDirection == Vertical) {
            mCurrentAction = "moveDown";
            AudioEvent audioEvent(Audio::Click);
            mEventManager.emit(audioEvent);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && mDirection == Horizontal) {
            mCurrentAction = "moveLeft";
            AudioEvent audioEvent(Audio::Click);
            mEventManager.emit(audioEvent);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && mDirection == Horizontal) {
            mCurrentAction = "moveRight";
            AudioEvent audioEvent(Audio::Click);
            mEventManager.emit(audioEvent);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            RequestStackEvent pauseEvent(StateManager::Push,States::Pause);
            mEventManager.emit(pauseEvent);
        }
    }
    void InputSystem::process(Player* gameActor, sf::Event& event) {
        mPreviousAction = mCurrentAction;
        createAction();
        if (mCurrentAction == "moveUp") {
            mDirection = Horizontal;
            AnimationEvent event(mCurrentAction, mPreviousAction);
            CommandPtr moveUp(std::make_unique<MoveUpCommand>(gameActor));
            notify(std::move(moveUp));
            mEventManager.emit(event);
        }
        else if (mCurrentAction == "moveDown") {
            mDirection = Horizontal;
            AnimationEvent event(mCurrentAction, mPreviousAction);
            CommandPtr moveDown(std::make_unique<MoveDownCommand>(gameActor));
            notify(std::move(moveDown));
            mEventManager.emit(event);
        }
        else if (mCurrentAction == "moveRight") {
            mDirection = Vertical;
            AnimationEvent event(mCurrentAction, mPreviousAction);
            CommandPtr moveRight(std::make_unique<MoveRightCommand>(gameActor));
            notify(std::move(moveRight));
            mEventManager.emit(event);
        }
        else if(mCurrentAction == "moveLeft") {
            mDirection = Vertical;
            AnimationEvent event(mCurrentAction, mPreviousAction);
            CommandPtr moveLeft(std::make_unique<MoveLeftCommand>(gameActor));
            notify(std::move(moveLeft));
            mEventManager.emit(event);
        }
        if (event.type == sf::Event::LostFocus) {
            RequestStackEvent pauseEvent(StateManager::Push, States::Pause);
            mEventManager.emit(pauseEvent);
        }
    }
}
