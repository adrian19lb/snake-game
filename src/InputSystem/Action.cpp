#include "../../include/InputSystem/Action.hpp"
namespace snake {
    template<typename ButtonCode>
    Action::ButtonWrapper<ButtonCode>::ButtonWrapper(ButtonCode code, ActionType action) :
        mButtonCode(code),
        mAction(action),
        mIsAlive(true) {
    }
    Action::Action(sf::Keyboard::Key key, ActionType action) :
        mKeyboard(key, action),
        mMouse() {
    }
    Action::Action(sf::Mouse::Button button, ActionType action) :
        mKeyboard(),
        mMouse(button, action) {
    }
    bool Action::isKeyboardEventOccured(typename Input<sf::Keyboard::Key>::Event& event) {
        if (mKeyboard.getType() == event.type && mKeyboard.getCode() == event.code) {
            return true;
        }
        else if (mKeyboard.getType() != event.type || mKeyboard.getCode() != event.code) {
            return false;
        }
    }
    bool Action::isMouseEventOccured(typename Input<sf::Mouse::Button>::Event& event) {
        if (mMouse.getType() == event.type && mMouse.getCode() == event.code) {
            return true;
        }
        else if (mMouse.getType() != event.type || mMouse.getCode() != event.code) {
            return false;
        }
    }
    bool Action::check() {
        auto keyEvent = mKeyboardEvents.createEvent();
        auto mouseEvent = mMouseEvents.createEvent();
        if (mKeyboard.isAlive()) {
            return isKeyboardEventOccured(keyEvent);
        }
        else if (mMouse.isAlive()) {
            return isMouseEventOccured(mouseEvent);
        }
    }
}
