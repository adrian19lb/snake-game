namespace snake {
    template<typename ButtonCode>
    Input<ButtonCode>::Event::Event(ActionType _type, ButtonCode _buttonCode) :
        type(_type),
        code(_buttonCode) {
    }
    template<typename ButtonCode>
    Input<ButtonCode>::Event::Event(const Event& event) {
        this->type = event.type;
        this->code = event.code;
    }
    template<typename ButtonCode>
    Input<ButtonCode>::Input() {
        mCurrentInputState = ActionType::None;
        mDevices[std::type_index(typeid(sf::Keyboard))] = [](int argument) {
          auto key = static_cast<sf::Keyboard::Key>(argument);
          return sf::Keyboard::isKeyPressed(key);
        };
        mDevices[std::type_index(typeid(sf::Mouse))] = [](int argument) {
          auto button = static_cast<sf::Mouse::Button>(argument);
          return sf::Mouse::isButtonPressed(button);
        };
    }
    template<typename ButtonCode>
    template<typename InputType>
    int Input<ButtonCode>::checkButtonPressed(int lowerRange, int upperRange) {
        auto& buttonCode = lowerRange;
        while (buttonCode != upperRange) {
            if (mDevices[std::type_index(typeid(InputType))](buttonCode)) {
                return buttonCode;
            }
            ++buttonCode;
        }
        return ActionType::None;
    }
    template<typename ButtonCode>
    int Input<ButtonCode>::checkButtonPressed() {
        int buttonCode;
        int lowerRange;
        int upperRange;
        if (std::type_index(typeid(ButtonCode)) == std::type_index(typeid(sf::Keyboard::Key))) {
            lowerRange = sf::Keyboard::A;
            upperRange = sf::Keyboard::KeyCount;
            buttonCode = checkButtonPressed<sf::Keyboard>(lowerRange, upperRange);
        }
        else if (std::type_index(typeid(ButtonCode)) == std::type_index(typeid(sf::Mouse::Button))) {
            lowerRange = sf::Mouse::Left;
            upperRange = sf::Mouse::ButtonCount;
            buttonCode = checkButtonPressed<sf::Mouse>(lowerRange, upperRange);
        }
        return buttonCode;
    }
    template<typename ButtonCode>
    void Input<ButtonCode>::updateInput() {
        mPreviousInputState = mCurrentInputState;
        mCurrentInputState = checkButtonPressed();
    }
    template<typename ButtonCode>
    typename Input<ButtonCode>::Event Input<ButtonCode>::createEvent() {
        updateInput();
        ButtonCode currentButtonCode = static_cast<ButtonCode>(mCurrentInputState);
        ButtonCode previousButtonCode = static_cast<ButtonCode>(mPreviousInputState);
        if (mPreviousInputState == ActionType::None && mCurrentInputState == ActionType::None) {
            Event nullEvent(ActionType::None, currentButtonCode);
            return nullEvent;
        }
        else if (mCurrentInputState == mPreviousInputState) {
            Event holdButtonEvent(ActionType::HoldButton, currentButtonCode);
            return holdButtonEvent;
        }
        else if (mCurrentInputState != mPreviousInputState && mCurrentInputState != ActionType::None) {
            Event downButtonEvent(ActionType::PressButton, currentButtonCode);
            return downButtonEvent;
        }
        else if (mCurrentInputState != mPreviousInputState && mCurrentInputState == ActionType::None) {
            Event releaseButtonEvent(ActionType::ReleaseButton, previousButtonCode);
            return releaseButtonEvent;
        }
    }
}
