#include "../../include/InputSystem/PlayerController.hpp"
namespace snake {
    PlayerController::PlayerController() :
        mConstMovementFlag(false),
        mControllerBuffer(5u) {
            Action pressUp(sf::Keyboard::Up, ActionType::PressButton);
            Action pressDown(sf::Keyboard::Down, ActionType::PressButton);
            Action pressRight(sf::Keyboard::Right, ActionType::PressButton);
            Action pressLeft(sf::Keyboard::Left, ActionType::PressButton);
            Action pressZ(sf::Keyboard::Z, ActionType::PressButton);
            Action pressX(sf::Keyboard::X, ActionType::PressButton);
            mActionBinder["moveLeft"] = pressLeft;
            mActionBinder["moveRight"] = pressRight;
            mActionBinder["moveUp"] = pressUp;
            mActionBinder["moveDown"] = pressDown;
            mActionBinder["decrease"] = pressZ;
            mActionBinder["increase"] = pressX;
    }
    void PlayerController::setConstantMovement(bool movementFlag) {
        mConstMovementFlag = movementFlag;
    }
    void PlayerController::setKeyMovementRepeated(const std::string& action) {
        if (mControllerBuffer.top() != nullptr) {
            previousAction = mControllerBuffer.top()->data;
            if (mControllerBuffer.top()->data == "moveUp") {
                if (action != "moveDown") {
                    mControllerBuffer.push(action);
                }
            }
            else if (mControllerBuffer.top()->data == "moveDown") {
                if (action != "moveUp") {
                    mControllerBuffer.push(action);
                }
            }
            else if (mControllerBuffer.top()->data == "moveLeft") {
                if (action != "moveRight") {
                    mControllerBuffer.push(action);
                }
            }
            else if (mControllerBuffer.top()->data == "moveRight") {
                if (action != "moveLeft") {
                    mControllerBuffer.push(action);
                }
            }
            else {
                mControllerBuffer.push(action);
            }
        }
        else if (mControllerBuffer.top() == nullptr) {
            mControllerBuffer.push(action);
        }
    }
    void PlayerController::checkEvent(sf::Event& event) {
        bool isOccured = false;
        for (auto& action : mActionBinder) {
            if (action.second.isEventOccured(event)) {
                if (mConstMovementFlag) {
                    setKeyMovementRepeated(action.first);
                }
                else if (!mConstMovementFlag) {
                    mControllerBuffer.push(action.first);
                }
                isOccured = true;
                break;
            }
        }
        if (!isOccured) {
            previousAction.clear();
        }
    }
    bool PlayerController::isActive(const std::string& action) {
        return mControllerBuffer.isActive(action);
    }
    void PlayerController::clearBuffer() {
        if (mConstMovementFlag)
            mControllerBuffer.clearBuffer();
        else if (!mConstMovementFlag)
            mControllerBuffer.pop();
    }
}
