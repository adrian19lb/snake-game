#include "../../include/MovementSystem/MovementSystem.hpp"
namespace snake {
    MovementSystem::MovementSystem() {
    }
    void MovementSystem::process() {
        for (const auto& command : mMoveCommand) {
            command->execute();
        }
        mMoveCommand.clear();
     }
    void MovementSystem::receive(CommandPtr& command) {
        mMoveCommand.emplace_back(std::move(command));

    }
}
