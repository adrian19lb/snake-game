#include "../../include/CreditsState/CreditsState.hpp"
namespace snake {
    CreditsState::CreditsState(StateManager& stateManager, GameContext context)
    : IState(stateManager, context)
    , mButton() {
        mBackground.setTexture(mGameContext.textures->get(Entity::Menu));
        sf::Vector2u buttonSize(250u, 75u);
        sf::Vector2f buttonPosition(mGameContext.gameWindow->getSize());
        mButton = std::move(std::make_unique<Button>(&mGameContext));
        mButton->create(buttonSize, "BACK");
        mButton->setPosition(buttonPosition.x/2 - 125u, buttonPosition.y - 100);

        std::ifstream creditsStream("doc/Credits.txt");
        if (!creditsStream) {
            throw std::runtime_error("Map::create - fail to load: doc/Credits.txt");
        }
        unsigned offset = 0;
        for (std::string line; std::getline(creditsStream, line);) {
            sf::Text text(line, mGameContext.fonts->get(Entity::Credits), 20u);
            text.setFillColor(sf::Color(50, 50, 50));
            text.setPosition(10.f, 35.f * offset);
            mCredits.emplace_back(text);
            offset++;
        }
        for (unsigned i = 0; i < 4; i++) {
            mCredits[2*i].setStyle(sf::Text::Bold);
            mCredits[2*i].setCharacterSize(30u);
        }
        mCredits[10].setStyle(sf::Text::Bold);
        mCredits[10].setCharacterSize(30u);
    }
    void CreditsState::handleInput(sf::Event& event) {
        if (mButton->isSelected() && mButton->isPressed()) {
            mStateManager->popState();
        }
    }
    void CreditsState::update(sf::Time dt) {
    }
    void CreditsState::draw() {
        mGameContext.gameWindow->draw(mBackground);
        for (auto& text : mCredits) {
            mGameContext.gameWindow->draw(text);
        }
        mButton->draw();
    }
}
