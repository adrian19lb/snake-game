#include "../../include/TailIncrementer/TailIncrementer.hpp"
namespace snake {
    void TailIncrementer::receive(CommandPtr& command) {
        mIncrementCommand = std::move(command);
        mIncrementCommand->execute();
        mIncrementCommand.reset();
    }
}
