#include "../../include/PauseState/PauseState.hpp"
namespace snake {
    PauseState::PauseState(StateManager& stateManager, GameContext context)
    : IState(stateManager, context) {
        mBackground.setTexture(mGameContext.textures->get(Entity::Pause));
        for (unsigned i = 0; i < 2; ++i) {
            mButtons.emplace_back(std::make_unique<Button>(&mGameContext));
        }
        sf::Vector2u buttonSize(250u, 75u);
        sf::Vector2f buttonPosition(mGameContext.gameWindow->getSize());
        mButtons[0]->create(buttonSize, "RESUME");
        mButtons[1]->create(buttonSize, "EXIT");

        mButtons[0]->setPosition(buttonPosition.x/2 - 125u, buttonPosition.y - 250);
        mButtons[1]->setPosition(buttonPosition.x/2 - 125u, buttonPosition.y - 175);

        mGameContext.musics->get(States::Pause).play();
        mGameContext.musics->get(States::Pause).setLoop(true);
    }
    void PauseState::handleInput(sf::Event& event) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            mGameContext.musics->get(States::Pause).stop();
            mGameContext.musics->get(States::Playing).play();
            mStateManager->popState();
        }
        else if (mButtons[0]->isSelected() && mButtons[0]->isPressed()) {
            mStateManager->popState();
            mGameContext.musics->get(States::Pause).stop();
            mGameContext.musics->get(States::Playing).play();
        }
        else if (mButtons[1]->isSelected() && mButtons[1]->isPressed()) {
            mStateManager->popState();
            mStateManager->popState();
            mStateManager->pushState(States::MainMenu);
            mGameContext.musics->get(States::Pause).stop();
        }
    }
    void PauseState::update(sf::Time dt) {
    }
    void PauseState::draw() {
        mGameContext.gameWindow->draw(mBackground);
        for (auto& button : mButtons) {
            button->draw();
        }
    }
}
