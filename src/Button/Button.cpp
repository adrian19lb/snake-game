#include "../../include/Button/Button.hpp"
namespace snake {

    Button::Button(IState::GameContext* gameContext)
    : mGameContext(gameContext)
    , components(mGameContext->fonts, mGameContext->sounds)
    , mIsButtonClick(false)
    , mIsButtonSelected(false)
    , mIsButtonSoundPlayed(false) {
    }

    void Button::create(const sf::Vector2u& size, const std::string& internalText, sf::Color textColor) {
        components.create(size, internalText, textColor);
    }

    bool Button::isPressed() {
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            mIsButtonClick = true;
        }

        else {
            mIsButtonClick = false;
        }

        return mIsButtonClick;
    }

    bool Button::isSelected() {
        sf::Vector2f mousePosition(sf::Mouse::getPosition(*mGameContext->gameWindow));
        sf::FloatRect spriteRect(getTransform().transformRect(sprite.getLocalBounds()));

        if (spriteRect.contains(mousePosition)) {
            mIsButtonSelected = true;
        }

        else {
            mIsButtonSelected = false;
        }

        return mIsButtonSelected;
    }

    void Button::playSound() {
        if (mIsButtonSelected) {
            components.playSelectSound();
        }

        else if (mIsButtonClick) {
            components.playClickSound();
        }

        mIsButtonSoundPlayed = false;
    }

    void Button::setTexture(const sf::Texture& texture) {
        components.setTexture(texture);
    }

    void Button::update() {
        if (mIsButtonSelected) {
            components.repaint();
        }

        else if (!mIsButtonSelected) {
            components.paint();
        }

        components.render();
        playSound();
    }

    void Button::draw() {
        update();
        sprite.setTexture(components.getTexture());

        mGameContext->gameWindow->draw(sprite, getTransform());
    }
}
