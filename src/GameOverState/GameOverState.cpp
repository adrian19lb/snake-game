#include "../../include/GameOverState/GameOverState.hpp"
namespace snake {
    GameOverState::GameOverState(StateManager& stateManager, GameContext context)
    : IState(stateManager, context) {
        mBackground.setTexture(mGameContext.textures->get(Entity::GameOver));
        for (unsigned i = 0; i < 2; ++i) {
            mButtons.emplace_back(std::make_unique<Button>(&mGameContext));
        }
        sf::Vector2u buttonSize(350, 75u);
        sf::Vector2f buttonPosition(mGameContext.gameWindow->getSize());
        mButtons[0]->create(buttonSize, "RETRY");
        mButtons[1]->create(buttonSize, "BACK TO MENU");
        mButtons[0]->setPosition(buttonPosition.x/2 - 150u, buttonPosition.y - 250u);
        mButtons[1]->setPosition(buttonPosition.x/2 - 150u, buttonPosition.y - 175u);
    }
    void GameOverState::handleInput(sf::Event& event) {
        if (mButtons[0]->isSelected() && mButtons[0]->isPressed()) {
            mStateManager->popState();
            mStateManager->popState();
            mStateManager->pushState(States::Playing);
        }
        else if (mButtons[1]->isSelected() && mButtons[1]->isPressed()) {
            mStateManager->popState();
            mStateManager->popState();
            mStateManager->pushState(States::MainMenu);
        }
    }
    void GameOverState::update(sf::Time dt) {
    }
    void GameOverState::draw() {
        mGameContext.gameWindow->draw(mBackground);
        for (auto& button : mButtons) {
            button->draw();
        }
    }
}
