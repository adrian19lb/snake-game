#include "../../include/Renderer/Renderer.hpp"
namespace snake {
    Renderer::Renderer(sf::RenderTarget* target) :
        mTarget(target) {
    }
    void Renderer::draw(sf::Drawable& entity) {
        mTarget->draw(entity);
    }
}
