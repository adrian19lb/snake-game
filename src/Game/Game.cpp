#include "../../include/Game/Game.hpp"
namespace snake {

    Game::~Game() {
    }

    Game::Game()
    : gameWindow(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Snake", sf::Style::Close)
    , timeStep(sf::seconds(TIME_STEP))
    , textures()
    , fonts()
    , sounds()
    , stateManager(IState::GameContext(&gameWindow, &textures, &sounds, &musics, &fonts)) {
        loadTexturesFromFiles();
        loadFontsFromFiles();
        loadSoundsFromFiles();
        loadMusicsFromFiles();
        registerStates();
        stateManager.pushState(States::MainMenu);
    }

    void Game::loadTexturesFromFiles() {
        textures.load(Entity::GameOver, "media/background/LoseScreen.png");
        textures.load(Entity::Win, "media/background/WinScreen.png");
        textures.load(Entity::Menu, "media/background/StartScreen.png");
        textures.load(Entity::Pause, "media/background/PauseScreen.png");
        textures.load(Entity::Player, "media/texture/player.png");
        textures.load(Entity::Apple, "media/texture/apple.png");
        textures.load(Entity::Map, "media/texture/grass.png");
        textures.load(Entity::MuteButton, "media/texture/mute.png");
        textures.load(Entity::UnmuteButton, "media/texture/unmute.png");
    }

    void Game::loadFontsFromFiles() {
        fonts.load(Entity::Button, "media/font/kenpixel_square.ttf"); //Sleeping
        fonts.load(Entity::Credits, "media/font/kenvector_future_thin.ttf"); //Sleeping
    }

    void Game::loadSoundsFromFiles() {
        sounds.load(Audio::Eating, "media/audio/eatSound.ogg");
        sounds.load(Audio::Dying, "media/audio/dieSound.ogg");
        sounds.load(Audio::Click, "media/audio/click.wav");
        sounds.load(Audio::Winning, "media/audio/winSound.ogg");
        sounds.load(Audio::SpawnItem, "media/audio/Toom Click.wav");
        sounds.load(Audio::ButtonSelect, "media/audio/mouseSelection.wav");
        sounds.load(Audio::ButtonClick, "media/audio/mouseClickSound.ogg");
    }

    void Game::loadMusicsFromFiles() {
        musics.load(States::MainMenu, "media/music/Tropical Adventure.ogg");
        musics.load(States::Pause, "media/music/Children games on the beach.ogg");
        musics.load(States::Playing, "media/music/12barsblues_practice.ogg");
    }

    void Game::registerStates() {
        stateManager.registerState<WinState>(States::Win);
        stateManager.registerState<MenuState>(States::MainMenu);
        stateManager.registerState<GameState>(States::Playing);
        stateManager.registerState<PauseState>(States::Pause);
        stateManager.registerState<GameOverState>(States::GameOver);
        stateManager.registerState<CreditsState>(States::Credits);
    }

    void Game::run() {
        runFixedTimeStepLoop();
    }

    void Game::runFixedTimeStepLoop() {
        sf::Time accumulator = sf::Time::Zero;
        sf::Clock timer;
        while (gameWindow.isOpen()) {
            stateManager.applyPendingChanges();

            accumulator += timer.restart();
            while (accumulator >= timeStep) {
                processInput();
                updateLogic();
                accumulator -= timeStep;
            }

            if (checkRequestToCloseWindow()) {
                gameWindow.close();
                break;
            }

            render();
        }
    }

    void Game::processInput() {
        sf::Event event;
        while (gameWindow.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                gameWindow.close();
            }
        }
        stateManager.top()->handleInput(event);
    }

    void Game::updateLogic() {
        stateManager.top()->update(timeStep);
    }

    bool Game::checkRequestToCloseWindow() {
        if (stateManager.isEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }

    void Game::render() {
        gameWindow.clear();
        stateManager.top()->draw();
        gameWindow.display();
    }
}
