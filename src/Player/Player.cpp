#include "../../include/Player/Player.hpp"
namespace snake {
    Player::Segment::Segment(const sf::Vector2f& coordinates, const TextureHolder& holder)
    : mCoordinates(coordinates) {
        mSprite.setPosition(coordinates);
        mSprite.setTextureRect(sf::IntRect(108, 0, 36, 36));
        mSprite.setTexture(holder.get(Entity::Player));
        mSprite.setOrigin(18.f, 18.f);
    }
    Player::Segment::Segment(const Segment& segment, const TextureHolder& holder) {
        mCoordinates = segment.mSprite.getPosition();
        mSprite.setPosition(mCoordinates);
        mSprite.setOrigin(18.f, 18.f);
    }
    Player::Segment::Segment(const Segment& segment) {
        mCoordinates = segment.mSprite.getPosition();
        mSprite.setPosition(mCoordinates);
        mSprite.setOrigin(18.f, 18.f);
    }
    void Player::increaseTail() {
        Player::SegmentPtr newSegment(std::make_unique<Player::Segment>(*mTail.back()));
        mTail.emplace_back(std::move(newSegment));
    }
    void Player::decreaseTail() {
        if (mTail.size() > 1) {
            mTail.pop_back();
        }
    }
    unsigned Player::getSize() const {
        return mTail.size();
    }
    const Entity::ID Player::getID() {
        return mID;
    }
    void Player::draw(sf::RenderTarget& target, sf::RenderStates state) const {
        for (const auto& segment : mTail) {
            state.transform *= getTransform();
            target.draw(segment->mSprite, state);
        }
    }
    Player::Segment& Player::operator[](unsigned segmentIndex) {
        return *mTail.at(segmentIndex);
    }
    Player::Segment& Player::getHead() {
        return *mTail.front();
    }
    Player::Segment& Player::getTail() {
        return *mTail.back();
    }
    void Player::makeMovement(const sf::Vector2f& movement) {
        auto head = std::move(mTail.back());
        mTail.pop_back();
        if (mTail.size() != 0) {
            head->mSprite.setPosition(mTail.front()->mSprite.getPosition());
        }
        head->mSprite.move(movement);
        mTail.emplace_front(std::move(head));
    }
    void Player::moveUp() {
        sf::Vector2f movement(0.f, -mMovementStep);
        makeMovement(movement);
    }
    void Player::moveDown() {
        sf::Vector2f movement(0.f, mMovementStep);
        makeMovement(movement);
    }
    void Player::moveLeft() {
        sf::Vector2f movement(-mMovementStep, 0.f);
        makeMovement(movement);
    }
    void Player::moveRight() {
        sf::Vector2f movement(mMovementStep, 0.f);
        makeMovement(movement);
    }
}
