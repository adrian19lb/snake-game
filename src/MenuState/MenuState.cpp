#include "../../include/MenuState/MenuState.hpp"

namespace snake {

    MenuState::~MenuState() {
    }

    MenuState::MenuState(StateManager& stateManager, GameContext context)
    : IState(stateManager, context)
    , menuButtons() {
        mBackground.setTexture(mGameContext.textures->get(Entity::Menu));
        createButtons();
        setButtonsGlobalPosition();
        playAndLoopMusic();
        setMuteOrUnmuteButtonTexture();
    }

    void MenuState::createButtons() {
        for (unsigned i = 0u; i < BUTTONS_AMOUNT; ++i) {
            menuButtons.emplace_back(std::make_unique<Button>(&mGameContext));
        }

        sf::Vector2u buttonSize(250u, 75u);
        menuButtons[PLAY]->create(buttonSize, "PLAY");
        menuButtons[CREDITS]->create(buttonSize, "CREDITS");
        menuButtons[QUIT]->create(buttonSize, "QUIT");
        menuButtons[MUTE]->create(sf::Vector2u(62u, 62u), "");
    }

    void MenuState::setButtonsGlobalPosition() {
        sf::Vector2f buttonPosition( (mGameContext.gameWindow->getSize().x / 2) - 125.f, mGameContext.gameWindow->getSize().y );

        menuButtons[PLAY]->setPosition(buttonPosition.x, buttonPosition.y - 250.f);
        menuButtons[CREDITS]->setPosition(buttonPosition.x, buttonPosition.y - 175.f);
        menuButtons[QUIT]->setPosition(buttonPosition.x, buttonPosition.y - 100.f);
        menuButtons[MUTE]->setPosition(10.f, 10.f);
    }

    void MenuState::playAndLoopMusic() {
        sf::Music& menuMusic = mGameContext.musics->get(States::MainMenu);
        menuMusic.play();
        menuMusic.setLoop(true);
    }

    void MenuState::setMuteOrUnmuteButtonTexture() {
        const sf::Music& menuMusic = mGameContext.musics->get(States::MainMenu);

        if (menuMusic.getVolume() == 100u) {
            const auto& texture = mGameContext.textures->get(Entity::UnmuteButton);
            menuButtons[MUTE]->setTexture(texture);
        }

        else if (menuMusic.getVolume() == 0u) {
            const auto& texture = mGameContext.textures->get(Entity::MuteButton);
            menuButtons[MUTE]->setTexture(texture);
        }
    }

    void MenuState::handleInput(sf::Event& event) {
        const sf::Music& menuMusic = mGameContext.musics->get(States::MainMenu);

        if (menuButtons[PLAY]->isSelected() && menuButtons[PLAY]->isPressed()) {
            activePlayButtonAction();
        }

        else if (menuButtons[CREDITS]->isSelected() && menuButtons[CREDITS]->isPressed()) {
            activeCreditsButtonAction();
        }

        else if (menuButtons[QUIT]->isSelected() && menuButtons[QUIT]->isPressed()) {
            activeQuitButtonsAction();
        }

        else if (menuButtons[MUTE]->isSelected() && menuButtons[MUTE]->isPressed() && menuMusic.getStatus() == sf::Music::Playing) {
            activeMuteButtonAction();
        }

        else if (menuButtons[MUTE]->isSelected() && menuButtons[MUTE]->isPressed() && menuMusic.getStatus() == sf::Music::Stopped) {
            activeUnMuteButtonAction();
        }

    }

    void MenuState::activePlayButtonAction() {
        sf::Music& menuMusic = mGameContext.musics->get(States::MainMenu);
        menuMusic.stop();
        mStateManager->popState();
        mStateManager->pushState(States::Playing);
    }

    void MenuState::activeCreditsButtonAction() {
        mStateManager->pushState(States::Credits);
    }

    void MenuState::activeQuitButtonsAction() {
        mStateManager->clearStates();
    }

    void MenuState::activeMuteButtonAction() {
        MusicHolder& musicHolder = *(mGameContext.musics);
        sf::Music& menuMusic = musicHolder.get(States::MainMenu);
        menuMusic.stop();
        musicHolder.mute();
        setMuteOrUnmuteButtonTexture();
    }

    void MenuState::activeUnMuteButtonAction() {
        MusicHolder& musicHolder = *(mGameContext.musics);
        sf::Music& menuMusic = musicHolder.get(States::MainMenu);
        menuMusic.play();
        musicHolder.unmute();
        setMuteOrUnmuteButtonTexture();
    }

    void MenuState::update(sf::Time dt) {
    }

    void MenuState::draw() {
        mGameContext.gameWindow->draw(mBackground);

        for (auto& button : menuButtons) {
            button->draw();
        }
    }
}
