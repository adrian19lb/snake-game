#include "../../include/AudioSystem/AudioSystem.hpp"
namespace snake {
    AudioSystem::AudioSystem(EventManager& eventManager, const SoundHolder& soundHolder)
    : mSoundHolder(soundHolder)
    , mEventManager(eventManager) {
        mEventManager.subscribe<AudioEvent>(this);
    }
    void AudioSystem::clearBuffer() {
        for (auto itr = mSoundsList.begin(); itr != mSoundsList.end(); itr++) {
            if ((*itr)->getStatus() == sf::Sound::Stopped) {
                mSoundsList.erase(itr);
            }
        }
    }
    void AudioSystem::createSound(Audio::ID audioID) {
        switch (audioID) {
        case Audio::Eating:
            mSoundsList.emplace(mSoundsList.end(), std::make_unique<sf::Sound>(mSoundHolder.get(Audio::Eating)));
            break;
        case Audio::Dying:
            mSoundsList.emplace(mSoundsList.end(), std::make_unique<sf::Sound>(mSoundHolder.get(Audio::Dying)));
            break;
        case Audio::Click:
            mSoundsList.emplace(mSoundsList.end(), std::make_unique<sf::Sound>(mSoundHolder.get(Audio::Click)));
            break;
        case Audio::SpawnItem:
            mSoundsList.emplace(mSoundsList.end(), std::make_unique<sf::Sound>(mSoundHolder.get(Audio::SpawnItem)));
            break;
        case Audio::Winning:
            mSoundsList.emplace(mSoundsList.end(), std::make_unique<sf::Sound>(mSoundHolder.get(Audio::Winning)));
            break;
        }
        mSoundsList.back()->play();
    }
    void AudioSystem::receive(AudioEvent& audioEvent) {
        createSound(audioEvent.getAudio());
    }
}
