#include "../../include/Map/Map.hpp"
namespace snake {
    void Map::create(sf::Vector2u tileSize) {
        std::vector<std::vector<int>> tiles;
        std::ifstream mapStream("media/map/Map.txt");
        if (!mapStream) {
            throw std::runtime_error("Map::create - fail to load: media/map/Map.txt");
        }
        for (std::string line; std::getline(mapStream, line);) {
            tiles.emplace_back(std::vector<int>());
            std::stringstream lineStream(line);
            std::move(std::istream_iterator<int>(lineStream), std::istream_iterator<int>(), std::back_inserter(tiles.back()));
        }
        unsigned height = tiles.size();
        mVertices.setPrimitiveType(sf::Quads);
        for (unsigned j = 0; j < height; ++j) {
            unsigned width = tiles[j].size();
            for (unsigned i = 0; i < width; ++i) {
                int tilesNumber = tiles[j][i];
                int tu = tilesNumber % (mTileset.getSize().x / tileSize.x);
                int tv = tilesNumber / (mTileset.getSize().x / tileSize.x);
                sf::Vertex quad[4];
                quad[0].position = sf::Vector2f(i * tileSize.x, j*tileSize.y);
                quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
                quad[2].position = sf::Vector2f( (i + 1) * tileSize.x, (j + 1) * tileSize.y);
                quad[3].position = sf::Vector2f( i * tileSize.x, (j + 1) * tileSize.y);

                quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv*tileSize.y);
                quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv*tileSize.y);
                quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1)*tileSize.y);
                quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1)*tileSize.y);
                for (unsigned k = 0; k < 4; ++k) {
                    mVertices.append(quad[k]);
                }
            }
        }
    }
    void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        states.texture = &mTileset;
        target.draw(mVertices, states);
    }
}
