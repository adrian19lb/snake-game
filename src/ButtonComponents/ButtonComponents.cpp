#include "../../include/Button/BasicButtonComponent.hpp"

namespace snake {

        ButtonComponents::~ButtonComponents() {
        }

        ButtonComponents::ButtonComponents(FontHolder* fontsHolder, SoundHolder* soundsHolder)
        : mFontsHolder(fontsHolder)
        , mSoundsHolder(soundsHolder) {
        }

        void ButtonComponents::render() {
            renderTexture.draw(models.text);
            renderTexture.draw(models.texture);
            renderTexture.display();
        }

        void ButtonComponents::create(const sf::Vector2u& size, const std::string& internalText, sf::Color textColor) {
            renderTexture.create(size.x, size.y);
            setText(size, internalText, textColor);
            setSounds();
        }

        void ButtonComponents::setText(const sf::Vector2u& size, const std::string& internalText, sf::Color textColor) {
            sf::Color grey = sf::Color(sf::Color(100, 100, 100));
            textColors.repainted = grey;
            textColors.basic = textColor;

            models.text.setString(internalText);
            models.text.setFont(mFontsHolder->get(Entity::Button));
            models.text.setCharacterSize(0.5 * size.y);
            models.text.setFillColor(textColor);
            models.text.setOrigin(models.text.getLocalBounds().width / 2, models.text.getLocalBounds().height / 2);
            models.text.setPosition(size.x / 2, size.y / 2 - 5u);
        }

        void ButtonComponents::setSounds() {
            sounds.click.setBuffer(mSoundsHolder->get(Audio::ButtonClick));
            sounds.click.setBuffer(mSoundsHolder->get(Audio::ButtonSelect));
        }

        void ButtonComponents::paint() {
            models.text.setFillColor(textColors.basic);
            renderTexture.clear(sf::Color(0, 0, 0, 0));
        }

        void ButtonComponents::repaint() {
            models.text.setFillColor(textColors.repainted);
            renderTexture.clear(sf::Color(200, 200, 200, 50));
        }

        const sf::Texture& ButtonComponents::getTexture() const {
            return renderTexture.getTexture();
        }

        void ButtonComponents::setTexture(const sf::Texture& texture) {
            models.texture.setTexture(texture);
        }

        void ButtonComponents::playSelectSound() {
            sounds.select.play();
        }

        void ButtonComponents::playClickSound() {
            sounds.click.play();
        }

}
