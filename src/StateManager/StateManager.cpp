#include "../../include/StateManager/StateManager.hpp"
namespace snake {
    StateManager::StateManager(IState::GameContext context)
    : mStatesStack()
    , mStatesFactory()
    , mPendingList()
    , mGameContext(context.gameWindow, context.textures, context.sounds, context.musics, context.fonts) {
    }
    StateManager::PendingChanges::PendingChanges(Action action, States::ID stateID)
    : mAction(action)
    , mStateID(stateID) {
    }
    unsigned StateManager::size() const {
        return mStatesStack.size();
    }
    bool StateManager::isEmpty() const {
        return mStatesStack.empty();
    }
    void StateManager::clearStates() {
        mPendingList.emplace_back(PendingChanges(Action::Clear));
    }
    IState* StateManager::top() const {
        return mStatesStack.back().get();
    }
    void StateManager::pushState(States::ID stateID) {
        mPendingList.emplace_back(PendingChanges(Action::Push, stateID));
    }
    void StateManager::popState() {
        mPendingList.emplace_back(PendingChanges(Action::Pop));
    }
    StatePtr StateManager::createState(States::ID stateID) {
        const auto& found = mStatesFactory.find(stateID);
        assert(found != mStatesFactory.end());
        return std::move(found->second());
    }
    void StateManager::applyPendingChanges() {
        for (auto& changes : mPendingList) {
            switch (changes.mAction) {
            case Action::Push:
                mStatesStack.emplace_back(std::move(createState(changes.mStateID)));
                break;
            case Action::Pop:
                mStatesStack.pop_back();
                break;
            case Action::Clear:
                mStatesStack.clear();
                break;
            }
        }
        mPendingList.clear();
    }
}
