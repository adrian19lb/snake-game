#include "../../include/GameState/GameState.hpp"
namespace snake {
    GameState::GameState(StateManager& stateManager, GameContext context)
    : IState(stateManager, context)
    , mWorld(context){
        mGameContext.musics->get(States::Playing).play();
        mGameContext.musics->get(States::Playing).setLoop(true);

    }
    void GameState::handleInput(sf::Event& event) {
        mWorld.processInput(event);
    }
    void GameState::update(sf::Time dt) {
        checkRequest();
        mWorld.processMovement();
        mWorld.checkRequestPlayerTeleporter();
        mWorld.processCollision();
        mWorld.updateAudio();
        mWorld.updateScores();
    }
    void GameState::draw() {
        mGameContext.gameWindow->draw(mWorld);
    }
    void GameState::checkRequest() {
        auto request = RequestStackEvent(mWorld.checkRequest());
        StateManager::Action actionID = request.getActionID();
        States::ID stateID = request.getStateID();
        switch (actionID) {
        case StateManager::Push:
            mGameContext.musics->get(States::Playing).stop();
            mStateManager->pushState(stateID);
            break;
        case StateManager::Pop:
            mStateManager->popState();
            break;
        case StateManager::Clear:
            mStateManager->clearStates();
            break;
        default:
            break;
        }
    }
}
