#include "../../include/CollisionSystem/CollisionSystem.hpp"

namespace snake {

    CollisionSystem::~CollisionSystem() {
    }

    CollisionSystem::CollisionSystem(sf::RenderWindow* gameWindow, EventManager& eventManager, Player* player, ItemHolder& itemHolder)
    : mEventManager(eventManager)
    , mPlayer(player)
    , mItemHolder(itemHolder)
    , mGameWindow(gameWindow) {
        mEventManager.subscribe<ItemSpawnEvent>(this);
    }

    void CollisionSystem::check() {
        if (isPlayerEatItem()) {
            sendPlayerTailIncrementNotification();
            sendNotification<AudioEvent>(Audio::Eating);
            sendNotification<CollisionEvent>(mPlayer->getID(), mItemHolder.getItem().getID());
        }

        else if (isPlayerHeadImpactTail()) {
            sendNotification<AudioEvent>(Audio::Dying);
            sendNotification<RequestStackEvent>(StateManager::Push, States::GameOver);
        }

        else if (isReachedWinSize()) {
            sendNotification<AudioEvent>(Audio::Winning);
            sendNotification<RequestStackEvent>(StateManager::Push, States::Win);
        }
    }

    bool CollisionSystem::isPlayerEatItem() const {
        const auto& head = mPlayer->getHead();
        auto& itemSprite = mItemHolder.getItem().getSprite();
        if (isSpriteContainPoint( head.mSprite, itemSprite.getPosition() )) {
            return true;
        }
        else {
           return false;
        }
    }

    bool CollisionSystem::isSpriteContainPoint(const sf::Sprite&  sprite, const sf::Vector2f& point) const {
        if (sprite.getGlobalBounds().contains(point)) {
            return true;
        }
        else {
            return false;
        }
    }

    void CollisionSystem::sendPlayerTailIncrementNotification() {
        CommandPtr incrementPlayerTail(std::make_unique<IncrementTailCommand>(mPlayer));
        notify(std::move(incrementPlayerTail));
    }

    void CollisionSystem::receive(ItemSpawnEvent& event) {
        if (isItemSpawnInPlayer()) {
            sendNotification<CollisionEvent>(mPlayer->getID(), mItemHolder.getItem().getID());
        }
        else if (!isItemSpawnInPlayer()) {
            sendNotification<ScoreEvent>();
            sendNotification<AudioEvent>(Audio::SpawnItem);
        }
    }

    void CollisionSystem::notify(CommandPtr command) {
        for (const auto& observer : mObservers) {
            observer->receive(command);
        }
    }

    bool CollisionSystem::isItemSpawnInPlayer() const {
        bool isItemAndPlayerCollision = false;

        auto& itemSprite = mItemHolder.getItem().getSprite();
        for (unsigned i = 0; i < mPlayer->getSize(); i++) {
            if (isSpriteContainPoint( (*mPlayer)[i].mSprite, itemSprite.getPosition() )) {
                isItemAndPlayerCollision = true;
                break;
            }
        }

        return isItemAndPlayerCollision;
    }

    bool CollisionSystem::isPlayerHeadImpactTail() const {
        bool isHeadAndTailCollision = false;

        auto& head = mPlayer->getHead();
        for (unsigned i = 0; i < mPlayer->getSize() - 1; i++) {
            if (isSpriteContainPoint( (*mPlayer)[i + 1].mSprite, head.mSprite.getPosition() )) {
                isHeadAndTailCollision = true;
                break;
            }
        }

        return isHeadAndTailCollision;
    }

    bool CollisionSystem::isReachedWinSize() {
        unsigned titlesNumber = (mGameWindow->getSize().x/36u) * (mGameWindow->getSize().y/36u);
        if (mPlayer->getSize() == titlesNumber) {
            return true;
        }
        else {
            return false;
        }
    }
}
