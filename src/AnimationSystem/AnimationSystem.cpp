#include "../../include/AnimationSystem/AnimationSystem.hpp"
namespace snake {
    AnimationSystem::AnimationSystem(Player* player, EventManager& eventManager, const TextureHolder& texture)
    : mEventManager(eventManager)
    , mPlayer(player)
    , mTextureHolder(texture) {
        mEventManager.subscribe<AnimationEvent>(this);
    }
    void AnimationSystem::rotate(AnimationEvent& event) {
        const auto& action = event.getAction();
        auto& head = mPlayer->getHead();
        auto& tail = mPlayer->getTail();
        float angle;
        if (action == "moveUp") {
            angle = 270.f;
        }
        else if (action == "moveDown") {
            angle = 90.f;
        }
        else if (action == "moveRight") {
            angle = 0.f;
        }
        else if (action == "moveLeft") {
            angle = 180.f;
        }
        head.mSprite.setRotation(angle);
        tail.mSprite.setRotation(angle);
    }
    void AnimationSystem::meshBasic() {
        auto& head = mPlayer->getHead();
        auto& tail = mPlayer->getTail();
        head.mSprite.setTextureRect(mSpriteRect.tail);
        tail.mSprite.setTextureRect(mSpriteRect.head);
        tail.mSprite.setTexture(mTextureHolder.get(Entity::Player));
        if (mPlayer->getSize() > 2) {
            head.mSprite.setTextureRect(mSpriteRect.middle);
            (*mPlayer)[mPlayer->getSize() - 2].mSprite.setTextureRect(mSpriteRect.tail);
        }
    }
    void AnimationSystem::meshCorners(AnimationEvent& event) {
        const auto& action = event.getAction();
        const auto& prevAction = event.getPrevAction();
        auto& head = mPlayer->getHead();
        if (action == "moveUp" && prevAction == "moveRight") {
            head.mSprite.setTextureRect(mSpriteRect.bottomLeft);
        }
        else if (action == "moveUp" && prevAction == "moveLeft") {
            head.mSprite.setTextureRect(mSpriteRect.topLeft);
        }
        else if (action == "moveDown" && prevAction == "moveRight") {
            head.mSprite.setTextureRect(mSpriteRect.topLeft);
        }
        else if (action == "moveDown" && prevAction == "moveLeft") {
            head.mSprite.setTextureRect(mSpriteRect.bottomLeft);
        }
        else if (action == "moveRight" && prevAction == "moveUp") {
            head.mSprite.setTextureRect(mSpriteRect.topLeft);
        }
        else if (action == "moveRight" && prevAction == "moveDown") {
            head.mSprite.setTextureRect(mSpriteRect.bottomLeft);
        }
        else if (action == "moveLeft" && prevAction == "moveUp") {
            head.mSprite.setTextureRect(mSpriteRect.bottomLeft);
        }
        else if (action == "moveLeft" && prevAction == "moveDown") {
            head.mSprite.setTextureRect(mSpriteRect.topLeft);
        }
    }
    void AnimationSystem::receive(AnimationEvent& animation) {
        rotate(animation);
        meshBasic();
        if (mPlayer->getSize() > 2) {
            meshCorners(animation);
        }
    }
}
